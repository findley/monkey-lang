# Monkey Interpreter
My implementation of Monkey from Thorsten Ball's [Writing an Interpreter In Go](https://interpreterbook.com/).
Except I followed along using Rust instead.

This repo includes a monkey CLI program, a monkey Rust library, a monkey npm
package (WASM + bindings), and a website playground <https://monkey.findley.dev>.

## Install

To build the CLI program you need rust/cargo
```
$ git clone https://gitlab.com/findley/monkey-lang.git
$ cd monkey-lang
$ cargo install --release --path .
```

## Usage

Execute a file
```
$ monkey myscript.mnky
```

Start a REPL
```
$ monkey --repl
```

## Using the npm package
The npm package is not published to the default npm registry, instead it's
published to a gitlab registry. To install it, first map the @findley scope to
the gitlab registry:

```
$ npm config set @findley:registry=https://gitlab.com/api/v4/packages/npm/
```

Then install the package `@findley/monkey` like normal.

```
$ npm install --save @findley/monkey
```

The package includes typescript declarations to help with usage.

## Building the WASM Package

### GitLab CI

When a new version tag (vX.X.X) is pushed, there will be a manual publish-wasm
job in the pipeline. When it's triggered, it will build the WASM package and
publish it with npm to this GitLab project's repository.

### Locally
To build the WASM package locally, first install wasm-pack (if not already
installed). The latest version at the time of writing (0.10) was seg-falting, so
I selected 0.9 instead.

```
$ cargo install wasm-pack@0.9.1
```

From the project root, use wasm-pack to build.

```
$ wasm-pack build --out-dir '../pkg' lib --target web -s findley -- --features wasm
```

At this point you can manually pack or link the npm package in ./pkg

## Deploying the website

The monkey playground website is built on each commit to the main branch as a
docker image. The image contains the built web bundle served with nginx. Each
time the docker image is built, it will install the latest version of the
@findley/monkey npm package. This docker image is pushed to the GitLab project's
container registry.

A manual GitLab CI job `deploy:website` can be triggered to use k8s to deploy
the newly built image to <https://monkey.findley.dev>.
