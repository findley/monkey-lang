use crate::T;

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Token {
    pub kind: TokenKind,
    pub start_pos: u32,
    pub end_pos: u32,
}

#[derive(PartialEq, Debug, Clone, Copy, Eq, Hash)]
#[repr(u8)]
pub enum TokenKind {
    Illegal,
    Error,
    Eof,

    // Identifiers and literals
    Ident,
    Int,
    String,

    // Operators
    Assign,
    Plus,
    Minus,
    Bang,
    Asterisk,
    Slash,
    Mod,

    Lt,
    Gt,

    Eq,
    NotEq,

    // Delimeters
    Comma,
    Colon,
    Semicolon,

    Lparen,
    Rparen,
    Lbrace,
    Rbrace,
    Lbrack,
    Rbrack,

    // Keywords
    Function,
    Let,
    True,
    False,
    If,
    Else,
    Return,
}

impl Token {
    pub fn new(kind: TokenKind, start_pos: u32, end_pos: u32) -> Token {
        Token {
            kind,
            start_pos,
            end_pos
        }
    }
}

impl Default for Token {
    fn default() -> Self {
        Token {
            kind: T![EOF],
            start_pos: 0,
            end_pos: 0,
        }
    }
}

pub fn keyword(id: &str) -> Option<TokenKind> {
    match id {
        "let" => Some(T![LET]),
        "fn" => Some(T![FN]),
        "true" => Some(T![TRUE]),
        "false" => Some(T![FALSE]),
        "if" => Some(T![IF]),
        "else" => Some(T![ELSE]),
        "return" => Some(T![RETURN]),
        _ => None
    }
}

#[macro_export]
macro_rules! T {
    [ILLEGAL] => { crate::lexer::TokenKind::Illegal };
    [ERROR] => { crate::lexer::TokenKind::Error };
    [EOF] => { crate::lexer::TokenKind::Eof };
    [ID] => { crate::lexer::TokenKind::Ident };
    [INT] => { crate::lexer::TokenKind::Int };
    [STR] => { crate::lexer::TokenKind::String };
    [=] => { crate::lexer::TokenKind::Assign };
    [+] => { crate::lexer::TokenKind::Plus };
    [-] => { crate::lexer::TokenKind::Minus };
    [!] => { crate::lexer::TokenKind::Bang };
    [*] => { crate::lexer::TokenKind::Asterisk };
    [/] => { crate::lexer::TokenKind::Slash };
    [%] => { crate::lexer::TokenKind::Mod };
    [<] => { crate::lexer::TokenKind::Lt };
    [>] => { crate::lexer::TokenKind::Gt };
    [==] => { crate::lexer::TokenKind::Eq };
    [!=] => { crate::lexer::TokenKind::NotEq };
    [,] => { crate::lexer::TokenKind::Comma };
    [:] => { crate::lexer::TokenKind::Colon };
    [;] => { crate::lexer::TokenKind::Semicolon };
    ['('] => { crate::lexer::TokenKind::Lparen };
    [')'] => { crate::lexer::TokenKind::Rparen };
    ['{'] => { crate::lexer::TokenKind::Lbrace };
    ['}'] => { crate::lexer::TokenKind::Rbrace };
    ['['] => { crate::lexer::TokenKind::Lbrack };
    [']'] => { crate::lexer::TokenKind::Rbrack };
    [FN] => { crate::lexer::TokenKind::Function };
    [LET] => { crate::lexer::TokenKind::Let };
    [TRUE] => { crate::lexer::TokenKind::True };
    [FALSE] => { crate::lexer::TokenKind::False };
    [IF] => { crate::lexer::TokenKind::If };
    [ELSE] => { crate::lexer::TokenKind::Else };
    [RETURN] => { crate::lexer::TokenKind::Return };
}
