mod token;

#[cfg(test)]
mod tests;

pub use token::{Token, TokenKind};
use std::str::Chars;

use crate::T;

const EOF_CHAR: char = '\0';

#[allow(dead_code)]
pub fn tokenize(input: &str) -> impl Iterator<Item = Token> + '_ {
    let mut lexer = Lexer::from_source(input);
    std::iter::from_fn(move || {
        let token = lexer.next();
        if token.kind != T![EOF] { Some(token) } else { None }
    })
}

pub struct Lexer<'source> {
    input: &'source str,
    position: u32,
    chars: Chars<'source>,
}

impl<'source> Lexer<'source> {
    pub fn from_source(s: &'source str) -> Self {
        let lexer = Self {
            input: s,
            position: 0,
            chars: s.chars(),
        };

        return lexer;
    }

    pub fn next(&mut self) -> Token {
        self.eat_while(is_whitespace);
        self.position = self.current_pos();

        let current = match self.bump() {
            Some(c) => c,
            None => return Token::new(T![EOF], self.position, self.position)
        };

        let token = match current {
            '=' => match self.first() {
                '=' => {
                    self.bump();
                    T![==]
                },
                _ => T![=],
            },
            ';' => T![;],
            ':' => T![:],
            '(' => T!['('],
            ')' => T![')'],
            ',' => T![,],
            '+' => T![+],
            '{' => T!['{'],
            '}' => T!['}'],
            '[' => T!['['],
            ']' => T![']'],
            '-' => T![-],
            '!' => match self.first() {
                '=' => {
                    self.bump();
                    T![!=]
                }
                _ => T![!],
            },
            '*' => T![*],
            '/' => T![/],
            '%' => T![%],
            '<' => T![<],
            '>' => T![>],
            '"' => self.string(),
            c if is_digit(c) => self.number(),
            c if is_id_char(c) => self.identifier_or_keyword(),
            _ => TokenKind::Illegal,
        };

        let token = Token::new(token, self.position, self.current_pos());
        self.position = self.current_pos();

        token
    }

    fn identifier_or_keyword(&mut self) -> TokenKind {
        self.eat_while(is_id_char);
        let s = &self.input[self.current_range()];

        match token::keyword(s) {
            Some(k) => k,
            None => T![ID],
        }
    }

    fn number(&mut self) -> TokenKind {
        self.eat_while(is_digit);
        T![INT]
    }

    fn string(&mut self) -> TokenKind {
        loop {
            if let Some(c) = self.bump() {
                match (c, self.first()) {
                    ('\\', '\\') | ('\\', '"') => {
                        self.bump();
                    }
                    ('"', _) => {
                        break;
                    }
                    _ => {}
                }
            } else {
                return T![EOF];
            }
        }

        T![STR]
    }

    fn first(&self) -> char {
        self.chars.clone().next().unwrap_or(EOF_CHAR)
    }

    fn is_eof(&self) -> bool {
        self.chars.as_str().is_empty()
    }

    fn bump(&mut self) -> Option<char> {
        self.chars.next()
    }

    fn eat_while(&mut self, mut predicate: impl FnMut(char) -> bool) {
        while predicate(self.first()) && !self.is_eof() {
            self.bump();
        }
    }

    fn current_pos(&self) -> u32 {
        self.input.len() as u32 - self.chars.as_str().len() as u32
    }

    fn current_range(&self) -> std::ops::Range<usize> {
        self.position as usize..self.current_pos() as usize
    }

    pub fn get_token_source(&self, token: &Token) -> &'source str {
        &self.input[token.start_pos as usize .. token.end_pos as usize]
    }
}

fn is_id_char(c: char) -> bool {
    matches!(c, 'A'..='Z' | 'a'..='z' | '_')
}

fn is_whitespace(c: char) -> bool {
    matches!(c, '\t' | '\n' | '\r' | ' ')
}

fn is_digit(c: char) -> bool {
    matches!(c, '0'..='9')
}
