use super::*;

use expect_test::{expect, Expect};

#[test]
fn symbols() {
    check_lexing(
        "=+(){},;%",
        expect![[r#"
            Token { kind: Assign, start_pos: 0, end_pos: 1 }
            Token { kind: Plus, start_pos: 1, end_pos: 2 }
            Token { kind: Lparen, start_pos: 2, end_pos: 3 }
            Token { kind: Rparen, start_pos: 3, end_pos: 4 }
            Token { kind: Lbrace, start_pos: 4, end_pos: 5 }
            Token { kind: Rbrace, start_pos: 5, end_pos: 6 }
            Token { kind: Comma, start_pos: 6, end_pos: 7 }
            Token { kind: Semicolon, start_pos: 7, end_pos: 8 }
            Token { kind: Mod, start_pos: 8, end_pos: 9 }
        "#]],
    );
}

#[test]
fn identifier() {
    check_lexing(
        "hello_world",
        expect![[r#"
            Token { kind: Ident, start_pos: 0, end_pos: 11 }
        "#]],
    );
}

#[test]
fn number() {
    check_lexing(
        "1234567890",
        expect![[r#"
            Token { kind: Int, start_pos: 0, end_pos: 10 }
        "#]],
    );
}

#[test]
fn string() {
    check_lexing(
        r#" "hello world!" "#,
        expect![[r#"
            Token { kind: String, start_pos: 1, end_pos: 15 }
        "#]],
    );
}

#[test]
fn string_with_escaped_quote() {
    check_lexing(
        r#" "{\"name\": \"david\"}" "#,
        expect![[r#"
            Token { kind: String, start_pos: 1, end_pos: 24 }
        "#]],
    );
}

#[test]
fn string_escaping_escaped_slash() {
    check_lexing(
        r#" "\\" "#,
        expect![[r#"
            Token { kind: String, start_pos: 1, end_pos: 5 }
        "#]],
    );
}

#[test]
fn string_escaping_tricky() {
    check_lexing(
        r#" "\\\"" "#,
        expect![[r#"
            Token { kind: String, start_pos: 1, end_pos: 7 }
        "#]],
    );
}

#[test]
fn array_brackets() {
    check_lexing(
        r#"[1, true, "three"]"#,
        expect![[r#"
            Token { kind: Lbrack, start_pos: 0, end_pos: 1 }
            Token { kind: Int, start_pos: 1, end_pos: 2 }
            Token { kind: Comma, start_pos: 2, end_pos: 3 }
            Token { kind: True, start_pos: 4, end_pos: 8 }
            Token { kind: Comma, start_pos: 8, end_pos: 9 }
            Token { kind: String, start_pos: 10, end_pos: 17 }
            Token { kind: Rbrack, start_pos: 17, end_pos: 18 }
        "#]],
    );
}

#[test]
fn hash_literal() {
    check_lexing(
        r#"{"name": "Jimmy", "age": 72, "band": "Led Zeppelin"}"#,
        expect![[r#"
            Token { kind: Lbrace, start_pos: 0, end_pos: 1 }
            Token { kind: String, start_pos: 1, end_pos: 7 }
            Token { kind: Colon, start_pos: 7, end_pos: 8 }
            Token { kind: String, start_pos: 9, end_pos: 16 }
            Token { kind: Comma, start_pos: 16, end_pos: 17 }
            Token { kind: String, start_pos: 18, end_pos: 23 }
            Token { kind: Colon, start_pos: 23, end_pos: 24 }
            Token { kind: Int, start_pos: 25, end_pos: 27 }
            Token { kind: Comma, start_pos: 27, end_pos: 28 }
            Token { kind: String, start_pos: 29, end_pos: 35 }
            Token { kind: Colon, start_pos: 35, end_pos: 36 }
            Token { kind: String, start_pos: 37, end_pos: 51 }
            Token { kind: Rbrace, start_pos: 51, end_pos: 52 }
        "#]],
    );
}

#[test]
fn script() {
    let input = r#"
        let five = 5;
        let ten = 10;

        let add = fn(x, y) {
          x + y;
        };

        let result = add(five, ten);
        !-/*5;
        5 < 10 > 5;

        if (5 < 10) {
            return true;
        } else {
            return false;
        }

        10 == 10;
        10 != 9;
    "#;
    check_lexing(
        input,
        expect![[r#"
            Token { kind: Let, start_pos: 9, end_pos: 12 }
            Token { kind: Ident, start_pos: 13, end_pos: 17 }
            Token { kind: Assign, start_pos: 18, end_pos: 19 }
            Token { kind: Int, start_pos: 20, end_pos: 21 }
            Token { kind: Semicolon, start_pos: 21, end_pos: 22 }
            Token { kind: Let, start_pos: 31, end_pos: 34 }
            Token { kind: Ident, start_pos: 35, end_pos: 38 }
            Token { kind: Assign, start_pos: 39, end_pos: 40 }
            Token { kind: Int, start_pos: 41, end_pos: 43 }
            Token { kind: Semicolon, start_pos: 43, end_pos: 44 }
            Token { kind: Let, start_pos: 54, end_pos: 57 }
            Token { kind: Ident, start_pos: 58, end_pos: 61 }
            Token { kind: Assign, start_pos: 62, end_pos: 63 }
            Token { kind: Function, start_pos: 64, end_pos: 66 }
            Token { kind: Lparen, start_pos: 66, end_pos: 67 }
            Token { kind: Ident, start_pos: 67, end_pos: 68 }
            Token { kind: Comma, start_pos: 68, end_pos: 69 }
            Token { kind: Ident, start_pos: 70, end_pos: 71 }
            Token { kind: Rparen, start_pos: 71, end_pos: 72 }
            Token { kind: Lbrace, start_pos: 73, end_pos: 74 }
            Token { kind: Ident, start_pos: 85, end_pos: 86 }
            Token { kind: Plus, start_pos: 87, end_pos: 88 }
            Token { kind: Ident, start_pos: 89, end_pos: 90 }
            Token { kind: Semicolon, start_pos: 90, end_pos: 91 }
            Token { kind: Rbrace, start_pos: 100, end_pos: 101 }
            Token { kind: Semicolon, start_pos: 101, end_pos: 102 }
            Token { kind: Let, start_pos: 112, end_pos: 115 }
            Token { kind: Ident, start_pos: 116, end_pos: 122 }
            Token { kind: Assign, start_pos: 123, end_pos: 124 }
            Token { kind: Ident, start_pos: 125, end_pos: 128 }
            Token { kind: Lparen, start_pos: 128, end_pos: 129 }
            Token { kind: Ident, start_pos: 129, end_pos: 133 }
            Token { kind: Comma, start_pos: 133, end_pos: 134 }
            Token { kind: Ident, start_pos: 135, end_pos: 138 }
            Token { kind: Rparen, start_pos: 138, end_pos: 139 }
            Token { kind: Semicolon, start_pos: 139, end_pos: 140 }
            Token { kind: Bang, start_pos: 149, end_pos: 150 }
            Token { kind: Minus, start_pos: 150, end_pos: 151 }
            Token { kind: Slash, start_pos: 151, end_pos: 152 }
            Token { kind: Asterisk, start_pos: 152, end_pos: 153 }
            Token { kind: Int, start_pos: 153, end_pos: 154 }
            Token { kind: Semicolon, start_pos: 154, end_pos: 155 }
            Token { kind: Int, start_pos: 164, end_pos: 165 }
            Token { kind: Lt, start_pos: 166, end_pos: 167 }
            Token { kind: Int, start_pos: 168, end_pos: 170 }
            Token { kind: Gt, start_pos: 171, end_pos: 172 }
            Token { kind: Int, start_pos: 173, end_pos: 174 }
            Token { kind: Semicolon, start_pos: 174, end_pos: 175 }
            Token { kind: If, start_pos: 185, end_pos: 187 }
            Token { kind: Lparen, start_pos: 188, end_pos: 189 }
            Token { kind: Int, start_pos: 189, end_pos: 190 }
            Token { kind: Lt, start_pos: 191, end_pos: 192 }
            Token { kind: Int, start_pos: 193, end_pos: 195 }
            Token { kind: Rparen, start_pos: 195, end_pos: 196 }
            Token { kind: Lbrace, start_pos: 197, end_pos: 198 }
            Token { kind: Return, start_pos: 211, end_pos: 217 }
            Token { kind: True, start_pos: 218, end_pos: 222 }
            Token { kind: Semicolon, start_pos: 222, end_pos: 223 }
            Token { kind: Rbrace, start_pos: 232, end_pos: 233 }
            Token { kind: Else, start_pos: 234, end_pos: 238 }
            Token { kind: Lbrace, start_pos: 239, end_pos: 240 }
            Token { kind: Return, start_pos: 253, end_pos: 259 }
            Token { kind: False, start_pos: 260, end_pos: 265 }
            Token { kind: Semicolon, start_pos: 265, end_pos: 266 }
            Token { kind: Rbrace, start_pos: 275, end_pos: 276 }
            Token { kind: Int, start_pos: 286, end_pos: 288 }
            Token { kind: Eq, start_pos: 289, end_pos: 291 }
            Token { kind: Int, start_pos: 292, end_pos: 294 }
            Token { kind: Semicolon, start_pos: 294, end_pos: 295 }
            Token { kind: Int, start_pos: 304, end_pos: 306 }
            Token { kind: NotEq, start_pos: 307, end_pos: 309 }
            Token { kind: Int, start_pos: 310, end_pos: 311 }
            Token { kind: Semicolon, start_pos: 311, end_pos: 312 }
        "#]],
    );
}

fn check_lexing(src: &str, expect: Expect) {
    let actual: String = tokenize(src)
        .map(|token| format!("{:?}\n", token))
        .collect();
    expect.assert_eq(&actual)
}
