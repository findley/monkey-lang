use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use super::value::Value;

#[derive(Debug, Clone)]
pub struct Env {
    env: Rc<RefCell<EnvData>>,
}

#[derive(Debug)]
struct EnvData {
    store: HashMap<String, Value>,
    outer: Option<Env>,
}

impl EnvData {
    pub fn new() -> Self {
        Self {
            store: HashMap::new(),
            outer: None,
        }
    }

    pub fn new_enclosed(outer: Env) -> Self {
        Self {
            store: HashMap::new(),
            outer: Some(outer),
        }
    }

    pub fn get(&self, name: &String) -> Option<Value> {
        match self.store.get(name) {
            Some(v) => Some(v.clone()),
            None => match &self.outer {
                Some(outer) => outer.get(name),
                None => None,
            },
        }
    }

    pub fn set(&mut self, name: &str, val: Value) {
        self.store.insert(name.to_string(), val);
    }

    pub fn declared_vars(&self) -> Vec<String> {
        self.store.keys().cloned().collect()
    }
}

impl Env {
    pub fn new() -> Self {
        Self {
            env: Rc::new(RefCell::new(EnvData::new())),
        }
    }

    pub fn new_enclosed(outer: Env) -> Self {
        Self {
            env: Rc::new(RefCell::new(EnvData::new_enclosed(outer))),
        }
    }

    pub fn get(&self, name: &String) -> Option<Value> {
        self.env.borrow().get(name)
    }

    pub fn set(&mut self, name: &str, val: Value) {
        self.env.borrow_mut().set(name, val);
    }

    pub fn declared_vars(&self) -> Vec<String> {
        self.env.borrow().declared_vars()
    }
}

impl PartialEq for Env {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.env, &other.env)
    }
}

impl Eq for Env {}
