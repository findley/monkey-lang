use std::collections::HashMap;

use super::*;
use crate::lexer::Lexer;
use crate::parser::Parser;

#[test]
fn eval_literal() {
    let cases = vec![
        ("5", "5"),
        ("10", "10"),
        ("true", "true"),
        ("false", "false"),
        ("\"hello world\"", r#""hello world""#),
        (r#" "\\\"" "#, r#""\"""#),
        ("[1, 2, 3]", "[1, 2, 3]"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_prefix_bang() {
    let cases = vec![
        ("!true", Value::Boolean(false)),
        ("!false", Value::Boolean(true)),
        ("!5", Value::Boolean(false)),
        ("!!true", Value::Boolean(true)),
        ("!!false", Value::Boolean(false)),
        ("!!5", Value::Boolean(true)),
    ];
    test_cases(cases);
}

#[test]
fn eval_prefix_minus() {
    let cases = vec![
        ("-5", Value::Integer(-5)),
        ("-10", Value::Integer(-10)),
        ("-0", Value::Integer(0)),
    ];
    test_cases(cases);
}

#[test]
fn eval_infix_integer() {
    let cases = vec![
        ("5 + 5 + 5 + 5 - 10", Value::Integer(10)),
        ("2 * 2 * 2 * 2 * 2", Value::Integer(32)),
        ("-50 + 100 + -50", Value::Integer(0)),
        ("5 * 2 + 10", Value::Integer(20)),
        ("5 + 2 * 10", Value::Integer(25)),
        ("20 + 2 * -10", Value::Integer(0)),
        ("50 / 2 * 2 + 10", Value::Integer(60)),
        ("2 * (5 + 10)", Value::Integer(30)),
        ("3 * 3 * 3 + 10", Value::Integer(37)),
        ("3 * (3 * 3) + 10", Value::Integer(37)),
        ("(5 + 10 * 2 + 15 / 3) * 2 + -10", Value::Integer(50)),
        ("15 % 3", Value::Integer(0)),
        ("16 % 3", Value::Integer(1)),
    ];
    test_cases(cases);
}

#[test]
fn eval_infix_bool() {
    let cases = vec![
        ("1 < 2", Value::Boolean(true)),
        ("1 > 2", Value::Boolean(false)),
        ("1 < 1", Value::Boolean(false)),
        ("1 > 1", Value::Boolean(false)),
        ("1 == 1", Value::Boolean(true)),
        ("1 != 1", Value::Boolean(false)),
        ("1 == 2", Value::Boolean(false)),
        ("1 != 2", Value::Boolean(true)),
        ("true == true", Value::Boolean(true)),
        ("false == false", Value::Boolean(true)),
        ("true == false", Value::Boolean(false)),
        ("true != false", Value::Boolean(true)),
        ("false != true", Value::Boolean(true)),
        ("(1 < 2) == true", Value::Boolean(true)),
        ("(1 < 2) == false", Value::Boolean(false)),
        ("(1 > 2) == true", Value::Boolean(false)),
        ("(1 > 2) == false", Value::Boolean(true)),
    ];
    test_cases(cases);
}

#[test]
fn eval_concat_string() {
    let cases = vec![
        (r#""hello" + " " + "world""#, Value::String("hello world".to_string())),
    ];
    test_cases(cases);
}

#[test]
fn eval_if() {
    let cases = vec![
        ("if (true) { 10 }", "10"),
        ("if (false) { 10 }", "null"),
        ("if (1) { 10 }", "10"),
        ("if (1 < 2) { 10 }", "10"),
        ("if (1 > 2) { 10 }", "null"),
        ("if (1 > 2) { 10 } else { 20 }", "20"),
        ("if (1 < 2) { 10 } else { 20 }", "10"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_return() {
    let cases = vec![
        ("return 10;", "10"),
        ("return 10; 9;", "10"),
        ("return 2 * 5; 9;", "10"),
        ("9; return 2 * 5; 9;", "10"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_nested_block_returns() {
    let src = r#"
    if (10 > 1) {
        if (10 > 1) {
            if (10 > 1) {
                return 20;
            }
            return 10;
        }
        return 1;
    }
    "#;

    test_cases_str(vec![(src, "20")]);
}

#[test]
fn eval_use_of_return_value_in_builtin() {
    let src = r#"
    let f = fn() {
        return "hi";
    }
    len(f());
    "#;

    test_cases_str(vec![(src, "2")]);
}

#[test]
fn eval_use_of_return_value() {
    let src = r#"
    let f = fn() {
        return 5;
    }
    f() + 5;
    "#;

    test_cases_str(vec![(src, "10")]);
}

#[test]
fn eval_error_handling() {
    let cases = vec![
        ("5 + true;", "Cannot use `+` with non-integer types"),
        ("5 + true; 5;", "Cannot use `+` with non-integer types"),
        (
            "-true",
            "The `-` prefix operator can only be used with integers",
        ),
        ("true + false;", "Cannot use `+` with non-integer types"),
        (
            "5; true + false; 5",
            "Cannot use `+` with non-integer types",
        ),
        (
            "if (10 > 1) { true + false; }",
            "Cannot use `+` with non-integer types",
        ),
        (
            r#"
            if (10 > 1) {
                if (10 > 1) {
                    return true + false;
                }

                return 1;
            }
            "#,
            "Cannot use `+` with non-integer types",
        ),
        ("foobar", "Undefined variable foobar"),
    ];

    test_cases_str(cases);
}

#[test]
fn eval_let_statements() {
    let cases = vec![
        ("let a = 5; a;", Value::Integer(5)),
        ("let a = 5 * 5; a;", Value::Integer(25)),
        ("let a = 5; let b = a; b;", Value::Integer(5)),
        (
            "let a = 5; let b = a; let c = a + b + 5; c;",
            Value::Integer(15),
        ),
    ];
    test_cases(cases);
}

#[test]
fn eval_function_object() {
    let val = eval_str("fn(x) { x + 2 };").unwrap();
    match val {
        Value::Function { params, .. } => {
            let param_names = params
                .iter()
                .map(|i| i.name.clone())
                .collect::<Vec<String>>();
            assert_eq!(param_names, vec!["x"]);
        }
        _ => assert!(
            false,
            "Expected value of function expression to be function"
        ),
    }
}

#[test]
fn function_closure() {
    let src = r#"
    let mul_factory = fn(f) {
        fn(x) {
            x * f;
        };
    };
    let triple = mul_factory(3);
    triple(2);
    "#;

    test_cases(vec![(src, Value::Integer(6))]);
}

#[test]
fn recursive_gc() {
    let src = r#"
    let counter = fn(x) {
      if (x > 100) {
        return true;
      } else {
        let foobar = 9999;
        counter(x + 1);
      }
    };
    counter(0);
    "#;

    test_cases_str(vec![(src, "true")]);
}

#[test]
fn basic_gc() {
    let src = r#"
    let bar = 5;
    let foo = fn(x) {
        let blah = 999;
        if (x) {
            true;
        } else {
            false;
        }
    };
    foo(1);
    foo(2);
    "#;

    test_cases(vec![(src, Value::Boolean(true))]);
}

#[test]
fn eval_function_application() {
    let cases = vec![
        (
            "let identity = fn(x) { x; }; identity(5);",
            "5",
        ),
        (
            "let identity = fn(x) { return x; }; identity(5);",
            "5",
        ),
        (
            "let double = fn(x) { x * 2; }; double(5);",
            "10",
        ),
        (
            "let add = fn(x, y) { x + y; }; add(1, 10);",
            "11",
        ),
        (
            "let add = fn(x, y) { x + y; }; add(1 + 10, add(100, 1000));",
            "1111",
        ),
        ("fn(x) { x; }(5)", "5"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_builtin_len() {
    let cases = vec![
        (r#"len("")"#, "0"),
        (r#"len("four")"#, "4"),
        (r#"len([])"#, "0"),
        (r#"len([1, 2, 3])"#, "3"),
        (r#"len("hello world")"#, "11"),
        (r#"len(1)"#, "The len function only accepts strings and arrays"),
        (r#"len("one", "two")"#, "The len function expects one arg, got 2"),
        (r#"len()"#, "The len function expects one arg, got 0"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_builtin_first() {
    let cases = vec![
        ("first([])", "null"),
        ("first([5, 6, 7])", "5"),
        ("first([5, 6, 7], true)", "The first function expects one arg, got 2"),
        ("first(true)", "The first function only accepts arrays"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_builtin_last() {
    let cases = vec![
        ("last([])", "null"),
        ("last([5, 6, 7])", "7"),
        ("last([5, 6, 7], true)", "The last function expects one arg, got 2"),
        ("last(true)", "The last function only accepts arrays"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_builtin_rest() {
    let cases = vec![
        ("rest([1,2,3,4])", "[2, 3, 4]"),
        ("rest(rest([1,2,3,4]))", "[3, 4]"),
        ("rest(rest(rest([1,2,3,4])))", "[4]"),
        ("rest(rest(rest(rest([1,2,3,4]))))", "[]"),
        ("rest([])", "[]"),
        ("rest([5, 6, 7], true)", "The rest function expects one arg, got 2"),
        ("rest(true)", "The rest function only accepts arrays"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_builtin_push() {
    let cases = vec![
        ("push([], 1)", "[1]"),
        ("push([1,2,3], 4)", "[1, 2, 3, 4]"),
        ("push()", "The push function expects two args, got 0"),
        ("push(true, 5)", "The push function accepts an array and a value"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_array_map() {
    let src = r#"
    let map = fn(arr, f) {
        let iter = fn (arr, accumulated) {
            if (len(arr) == 0) {
                accumulated
            } else {
                iter(rest(arr), push(accumulated, f(first(arr))));
            }
        };
        iter(arr, []);
    };

    let a = [1, 2, 3, 4];
    let double = fn(x) { x * 2 };
    map(a, double);
    "#;

    test_cases_str(vec![(src, "[2, 4, 6, 8]")]);
}

#[test]
fn eval_array() {
    let cases = vec![
        ("[]", "[]"),
        ("[1, true, \"three\"]", "[1, true, \"three\"]"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_array_index() {
    let cases = vec![
        ("[1, 2, 3][0]", "1"),
        ("[1, 2, 3][1]", "2"),
        ("[1, 2, 3][2]", "3"),
        ("let i = 0; [1][i];", "1"),
        ("[1, 2, 3][1 + 1];", "3"),
        ("let myArray = [1, 2, 3]; myArray[2];", "3"),
        ("let myArray = [1, 2, 3]; myArray[0] + myArray[1] + myArray[2];", "6"),
        ("let myArray = [1, 2, 3]; let i = myArray[0]; myArray[i]", "2"),
        ("[1, 2, 3][3]", "null"),
        ("[1, 2, 3][-1]", "null"),
        ("5[1]", "Only hashes and arrays may be indexed"),
        ("[true][\"hello\"]", "Arrays may only be index by integer values"),
        ("5[true]", "Only hashes and arrays may be indexed"),
    ];
    test_cases_str(cases);
}

#[test]
fn value_hash() {
    let mut hash = HashMap::<Value, Value>::new();

    let name_1 = Value::String("name".to_string());
    let name_2 = Value::String("name".to_string());

    hash.insert(name_1, Value::Integer(11));

    assert!(hash.get(&name_2).unwrap() == &Value::Integer(11));
}

#[test]
fn hash_literal() {
    let src = r#"
    let two = "two";
    {
        "one": 10 - 9,
        two: 1 + 1,
        "thr" + "ee": 6 / 2,
        4: 4,
        true: 5,
        false: 6
    };
    "#;

    let expected: HashMap<Value, Value> = HashMap::from([
        (Value::String("one".to_string()), Value::Integer(1)),
        (Value::String("two".to_string()), Value::Integer(2)),
        (Value::String("three".to_string()), Value::Integer(3)),
        (Value::Integer(4), Value::Integer(4)),
        (Value::Boolean(true), Value::Integer(5)),
        (Value::Boolean(false), Value::Integer(6)),
    ]);

    let actual = eval_str(src).unwrap();

    if let Value::Hash(h) = actual {
        assert!(expected == *h);
    } else {
        assert!(false, "The source didn't produce a Hash value as expected. Got: {:?}", actual);
    }
}

#[test]
fn deep_array_equality() {
    let cases = vec![
        ("[1,2,3] == [1,2,3]", "true"),
    ];
    test_cases_str(cases);
}

#[test]
fn deep_hash_equality() {
    let cases = vec![
        ("{\"hello\": 5} == {\"hello\": 5}", "true"),
    ];
    test_cases_str(cases);
}

#[test]
fn eval_hash_index() {
    let cases = vec![
        (r#"{"foo": 5}["foo"]"#, "5"),
        (r#"{"foo": 5}["bar"]"#, "null"),
        (r#"let key = "foo"; {"foo": 5}[key]"#, "5"),
        (r#"{}["foo"]"#, "null"),
        (r#"{5: 5}[5]"#, "5"),
        (r#"{true: 5}[true]"#, "5"),
        (r#"{false: 5}[false]"#, "5"),
        ("5[1]", "Only hashes and arrays may be indexed"),
        (r#"{"foo": 5}[[true]]"#, "Hashes can only be indexed by integers, strings, or booleans"),
    ];
    test_cases_str(cases);
}

fn test_cases(cases: Vec<(&str, Value)>) {
    cases.iter().for_each(|(src, expected)| {
        let actual = eval_str(src).unwrap();
        assert!(
            actual == *expected,
            "\nexpected: {}\ngot: {}",
            expected,
            actual
        );
    });
}

fn test_cases_str(cases: Vec<(&str, &str)>) {
    cases.iter().for_each(|(src, expected)| {
        let actual = eval_str(src).map(|v| v.to_string()).unwrap_or_else(|err| err.first_message());
        assert!(
            actual == *expected,
            "\nexpected: {}\ngot: {}",
            expected,
            actual
        );
    });
}

fn eval_str(src: &str) -> Result<Value> {
    let mut parser = Parser::new(Lexer::from_source(src));
    let program = parser.parse_program();
    let mut env = Env::new();
    let eval = Evaluator::new();
    eval.eval(&Ast::Program(program), &mut env)
}
