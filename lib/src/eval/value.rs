use std::collections::HashMap;
use std::fmt::Display;
use std::hash::Hash;
use std::rc::Rc;

use crate::parser::ast;
use crate::parser::ast::Identifier;

use super::builtin::BuiltinKind;
use super::Env;

#[derive(Debug, Clone)]
pub enum Value {
    Integer(i64),
    Boolean(bool),
    String(String),
    Function {
        params: Rc<Vec<Identifier>>,
        body: Rc<ast::Statement>,
        closure: Env,
    },
    Array(Rc<Vec<Value>>),
    Hash(Rc<HashMap<Value, Value>>),
    Builtin(BuiltinKind),
    Return(Box<Value>),
    Null,
}

impl Value {
    pub fn is_truthy(&self) -> bool {
        match self {
            Self::Integer(i) => *i > 0,
            Self::Boolean(b) => *b,
            Self::Function { .. } => true,
            Self::String(_) => true,
            Self::Array(_) => true,
            Self::Hash(_) => true,
            Self::Builtin(_) => true,
            Self::Return(v) => v.is_truthy(),
            Self::Null => false,
        }
    }

    pub fn is_hashable(&self) -> bool {
        match self {
            Self::Integer(_) | Self::String(_) | Self::Boolean(_) => true,
            _ => false,
        }
    }

    pub fn unreturn(self) -> Value {
        match self {
            Self::Return(v) => *v,
            _ => self,
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Integer(v1), Self::Integer(v2)) => v1 == v2,
            (Self::Boolean(v1), Self::Boolean(v2)) => v1 == v2,
            (Self::String(v1), Self::String(v2)) => v1 == v2,
            (Self::Null, Self::Null) => true,
            (Self::Array(v1), Self::Array(v2)) => v1 == v2,
            (Self::Hash(v1), Self::Hash(v2)) => v1 == v2,
            (Self::Builtin(b1), Self::Builtin(b2)) => b1 == b2,
            (
                Self::Function {
                    body: body1,
                    closure: closure1,
                    ..
                },
                Self::Function {
                    body: body2,
                    closure: closure2,
                    ..
                },
            ) => Rc::ptr_eq(&body1, &body2) && closure1 == closure2,
            (Self::Return(v1), v2) => v2.eq(v1),
            (v1, Self::Return(v2)) => v1.eq(v2),
            _ => false,
        }
    }
}

impl Eq for Value {}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Integer(v) => write!(f, "{}", v),
            Self::Boolean(v) => {
                if *v {
                    write!(f, "true")
                } else {
                    write!(f, "false")
                }
            }
            Self::String(s) => write!(f, "\"{}\"", s),
            Self::Function {
                params, closure, ..
            } => {
                let params_vars: String = params
                    .iter()
                    .map(|i| i.name.clone())
                    .collect::<Vec<String>>()
                    .join(", ");
                let closure_vars: String = closure.declared_vars().join(", ");
                write!(f, "fn ({params_vars}) using ({closure_vars})")
            }
            Self::Array(a) => {
                write!(f, "[")?;
                write!(
                    f,
                    "{}",
                    a.iter()
                        .map(|v| v.to_string())
                        .collect::<Vec<String>>()
                        .join(", ")
                )?;
                write!(f, "]")
            }
            Self::Hash(h) => {
                write!(f, "{{")?;
                let body = h
                    .iter()
                    .map(|(k, v)| format!("{k}: {v}"))
                    .collect::<Vec<String>>()
                    .join(", ");
                write!(f, "{}", body)?;
                write!(f, "}}")
            }
            Self::Builtin(name) => write!(f, "builtin: {:?}", name),
            Self::Return(v) => write!(f, "{}", v),
            Self::Null => write!(f, "null"),
        }
    }
}

impl Hash for Value {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            Self::Integer(v) => state.write_i64(*v),
            Self::String(v) => state.write(v.as_bytes()),
            Self::Boolean(v) => state.write_u8(*v as u8),
            v => panic!("Disallowed Value variant used as hash key. Only Integer, String, and Boolean may be used as hash keys. Found {:?}", v),
        }
    }
}
