use std::rc::Rc;

use super::value::Value;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum BuiltinKind {
    Len,
    First,
    Last,
    Rest,
    Push,
    Puts,
}

pub struct Builtins {
    printer: Option<Box<dyn Fn(&str)>>,
}

impl Builtins {
    pub fn new() -> Self {
        Self { printer: None }
    }

    pub fn lookup(fn_name: &str) -> Option<BuiltinKind> {
        match fn_name {
            "len" => Some(BuiltinKind::Len),
            "first" => Some(BuiltinKind::First),
            "last" => Some(BuiltinKind::Last),
            "rest" => Some(BuiltinKind::Rest),
            "push" => Some(BuiltinKind::Push),
            "puts" => Some(BuiltinKind::Puts),
            _ => None,
        }
    }

    pub fn call(&self, builtin: BuiltinKind, args: &[Value]) -> Result<Value, String> {
        match builtin {
            BuiltinKind::Len => self.len(args),
            BuiltinKind::First => self.first(args),
            BuiltinKind::Last => self.last(args),
            BuiltinKind::Rest => self.rest(args),
            BuiltinKind::Push => self.push(args),
            BuiltinKind::Puts => self.puts(args),
        }
    }

    pub fn set_printer(&mut self, printer: Box<dyn Fn(&str)>) {
        self.printer = Some(printer);
    }

    fn len(&self, args: &[Value]) -> Result<Value, String> {
        if args.len() != 1 {
            return Err(format!(
                "The len function expects one arg, got {}",
                args.len()
            ));
        }

        match args {
            [Value::String(v)] => Ok(Value::Integer(v.len() as i64)),
            [Value::Array(v)] => Ok(Value::Integer(v.len() as i64)),
            _ => Err("The len function only accepts strings and arrays".into()),
        }
    }

    fn first(&self, args: &[Value]) -> Result<Value, String> {
        if args.len() != 1 {
            return Err(format!(
                "The first function expects one arg, got {}",
                args.len()
            ));
        }

        match args {
            [Value::Array(v)] => Ok(v.get(0).map(|e| e.clone()).unwrap_or(Value::Null)),
            _ => Err("The first function only accepts arrays".into()),
        }
    }

    fn last(&self, args: &[Value]) -> Result<Value, String> {
        if args.len() != 1 {
            return Err(format!(
                "The last function expects one arg, got {}",
                args.len()
            ));
        }

        match args {
            [Value::Array(v)] => Ok(v.last().map(|e| e.clone()).unwrap_or(Value::Null)),
            _ => Err("The last function only accepts arrays".into()),
        }
    }

    fn rest(&self, args: &[Value]) -> Result<Value, String> {
        if args.len() != 1 {
            return Err(format!(
                "The rest function expects one arg, got {}",
                args.len()
            ));
        }

        match args {
            [Value::Array(v)] => Ok(Value::Array(Rc::new(v.iter().cloned().skip(1).collect()))),
            _ => Err("The rest function only accepts arrays".into()),
        }
    }

    fn push(&self, args: &[Value]) -> Result<Value, String> {
        if args.len() != 2 {
            return Err(format!(
                "The push function expects two args, got {}",
                args.len()
            ));
        }

        match args {
            [Value::Array(v), new] => {
                let mut new_v = v.iter().cloned().collect::<Vec<Value>>();
                new_v.push(new.clone());
                Ok(Value::Array(Rc::new(new_v)))
            }
            _ => Err("The push function accepts an array and a value".into()),
        }
    }

    fn puts(&self, args: &[Value]) -> Result<Value, String> {
        for arg in args.iter() {
            match arg {
                Value::String(v) => match &self.printer {
                    Some(printer) => printer(format!("{v}").as_str()),
                    None => println!("{v}"),
                }
                _ => match &self.printer {
                    Some(printer) => printer(format!("{arg}").as_str()),
                    None => println!("{arg}"),
                }
            };
        }

        Ok(Value::Null)
    }
}
