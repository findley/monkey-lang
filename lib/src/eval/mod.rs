mod builtin;
mod env;
mod value;

#[cfg(test)]
mod tests;

use std::collections::HashMap;
use std::rc::Rc;

use crate::error::Error;
use crate::lexer::TokenKind;
use crate::parser::ast::{Ast, Expression, Identifier, Program, Statement};
use crate::Result;
use crate::T;

pub use env::Env;
pub use value::Value;

use self::builtin::Builtins;

pub struct Evaluator {
    builtins: Builtins,
}

impl Evaluator {
    pub fn new() -> Self {
        Evaluator {
            builtins: Builtins::new(),
        }
    }

    pub fn set_printer(&mut self, printer: Box<dyn Fn(&str)>) {
        self.builtins.set_printer(printer);
    }

    pub fn eval(&self, node: &Ast, env: &mut Env) -> Result<Value> {
        Ok(match node {
            Ast::Program(prog) => self.eval_prog(prog, env)?,
            Ast::Statement(stmt) => self.eval_stmt(&stmt, env)?,
            Ast::Expression(expr) => self.eval_expr(&expr, env)?,
            _ => Value::Null,
        })
    }

    fn eval_prog(&self, prog: &Program, env: &mut Env) -> Result<Value> {
        self.eval_block(&prog.statements, env)
    }

    fn eval_block(&self, block: &Vec<Rc<Statement>>, env: &mut Env) -> Result<Value> {
        let mut val = Value::Null;

        for stmt in block.iter() {
            val = self.eval_stmt(stmt, env)?;

            if let Value::Return(_) = val {
                return Ok(val);
            }
        }

        return Ok(val);
    }

    fn eval_stmt(&self, stmt: &Statement, env: &mut Env) -> Result<Value> {
        match stmt {
            Statement::BlockStatement { statements } => self.eval_block(statements, env),
            Statement::ExpressionStatement(expr) => self.eval_expr(expr, env),
            Statement::ReturnStatement(expr) => Ok(Value::Return(Box::new(self.eval_expr(expr, env)?))),
            Statement::LetStatement { name, value, .. } => self.eval_let(name, value, env),
        }
    }

    fn eval_let(&self, name: &String, val_expr: &Rc<Expression>, env: &mut Env) -> Result<Value> {
        let val = self.eval_expr(val_expr, env)?;
        env.set(name, val.clone());

        Ok(val)
    }

    fn eval_expr(&self, expr: &Rc<Expression>, env: &mut Env) -> Result<Value> {
        match &**expr {
            Expression::IntExpression { value, .. } => Ok(Value::Integer(*value)),
            Expression::BooleanExpression { value, .. } => Ok(Value::Boolean(*value)),
            Expression::StringExpression { value, .. } => Ok(Value::String(value.clone())),
            Expression::ArrayExpression { elements } => self.eval_array(elements, env),
            Expression::HashExpression { pairs } => self.eval_hash(pairs, env),
            Expression::IndexExpression { left, index } => self.eval_index(left, index, env),
            Expression::FunctionExpression { params, body } => self.eval_fn(params, body, env),
            Expression::PrefixExpression { op, right } => self.eval_prefix(op.kind, right, env),
            Expression::InfixExpression { op, left, right } => {
                self.eval_infix(op.kind, left, right, env)
            }
            Expression::IdentifierExpression(id) => self.eval_id(&id.name, env),
            Expression::CallExpression { function, args } => self.eval_call(function, args, env),
            Expression::IfExpression {
                condition,
                consequence,
                alternative,
            } => self.eval_if(&condition, &consequence, alternative.as_ref(), env),
            _ => Ok(Value::Null),
        }
    }

    fn eval_array(&self, el_exprs: &Vec<Rc<Expression>>, env: &mut Env) -> Result<Value> {
        let elements: Result<Vec<Value>> = el_exprs
            .iter()
            .map(|a| self.eval_expr(a, env).map(|v| v.unreturn()))
            .collect();

        Ok(Value::Array(Rc::new(elements?)))
    }

    fn eval_hash(
        &self,
        pairs: &Vec<(Rc<Expression>, Rc<Expression>)>,
        env: &mut Env,
    ) -> Result<Value> {
        let map: Result<HashMap<Value, Value>> = pairs
            .iter()
            .map(|(key_expr, value_expr)| {
                let k = self.eval_expr(key_expr, env)?;
                if !k.is_hashable() {
                    return Err(Error::eval(
                        "Hash keys must be integer, string, or boolean.",
                    ));
                }
                let v = self.eval_expr(value_expr, env)?;
                Ok((k, v))
            })
            .collect();

        Ok(Value::Hash(Rc::new(map?)))
    }

    fn eval_index(
        &self,
        left: &Rc<Expression>,
        index: &Rc<Expression>,
        env: &mut Env,
    ) -> Result<Value> {
        let arr = self.eval_expr(left, env)?;
        let idx = self.eval_expr(index, env)?;
        Ok(match (arr, idx) {
            (Value::Array(v), Value::Integer(i)) => {
                v.get(i as usize).map(|v| v.clone()).unwrap_or(Value::Null)
            }
            (Value::Hash(h), k) => {
                if k.is_hashable() {
                    h.get(&k).map(|v| v.clone()).unwrap_or(Value::Null)
                } else {
                    return Err(Error::eval(
                        "Hashes can only be indexed by integers, strings, or booleans",
                    ));
                }
            }
            (Value::Array(_), _) => {
                return Err(Error::eval("Arrays may only be index by integer values"));
            }
            (_, _) => {
                return Err(Error::eval("Only hashes and arrays may be indexed"));
            }
        })
    }

    fn eval_fn(
        &self,
        params: &Rc<Vec<Identifier>>,
        body: &Rc<Statement>,
        env: &mut Env,
    ) -> Result<Value> {
        Ok(Value::Function {
            params: params.clone(),
            body: body.clone(),
            closure: env.clone(),
        })
    }

    fn eval_prefix(&self, op: TokenKind, right: &Rc<Expression>, env: &mut Env) -> Result<Value> {
        let rval = self.eval_expr(right, env)?.unreturn();
        let v = match op {
            T![!] => match rval {
                Value::Boolean(v) => Value::Boolean(!v),
                Value::Integer(v) => Value::Boolean(!(v > 0)),
                Value::Null => Value::Boolean(true),
                Value::Function { .. } | Value::Builtin(_) => {
                    return Err(Error::eval("Cannot use `!` prefix on a function value"));
                }
                Value::String(_) => {
                    return Err(Error::eval("Cannot use `!` prefix on a string value"));
                }
                Value::Array(_) => {
                    return Err(Error::eval("Cannot use `!` prefix on an array value"));
                }
                Value::Hash(_) => {
                    return Err(Error::eval("Cannot use `!` prefix on a hash value"));
                }
                Value::Return(_) => {
                    return Err(Error::eval("Cannot use `!` on return values (???)"));
                }
            },
            T![-] => match rval {
                Value::Integer(v) => Value::Integer(-v),
                _ => {
                    return Err(Error::eval(
                        "The `-` prefix operator can only be used with integers",
                    ));
                }
            },
            _ => return Err(Error::eval(format!("Unexpected prefix operator: {:?}", op))),
        };

        Ok(v)
    }

    fn eval_infix(
        &self,
        op: TokenKind,
        left: &Rc<Expression>,
        right: &Rc<Expression>,
        env: &mut Env,
    ) -> Result<Value> {
        let lval = self.eval_expr(left, env)?.unreturn();
        let rval = self.eval_expr(right, env)?.unreturn();
        let v = match op {
            T![+] => match (lval, rval) {
                (Value::Integer(l), Value::Integer(r)) => Value::Integer(l + r),
                (Value::String(l), Value::String(r)) => Value::String(l + &r),
                _ => {
                    return Err(Error::eval("Cannot use `+` with non-integer types"));
                }
            },
            T![-] => match (lval, rval) {
                (Value::Integer(l), Value::Integer(r)) => Value::Integer(l - r),
                _ => {
                    return Err(Error::eval("Cannot use `-` with non-integer types"));
                }
            },
            T![*] => match (lval, rval) {
                (Value::Integer(l), Value::Integer(r)) => Value::Integer(l * r),
                _ => {
                    return Err(Error::eval("Cannot use `*` with non-integer types"));
                }
            },
            T![/] => match (lval, rval) {
                (Value::Integer(l), Value::Integer(r)) => Value::Integer(l / r),
                _ => {
                    return Err(Error::eval("Cannot use `/` with non-integer types"));
                }
            },
            T![%] => match (lval, rval) {
                (Value::Integer(l), Value::Integer(r)) => Value::Integer(l % r),
                _ => {
                    return Err(Error::eval("Cannot use `%` with non-integer types"));
                }
            },
            T![<] => match (lval, rval) {
                (Value::Integer(l), Value::Integer(r)) => Value::Boolean(l < r),
                _ => {
                    return Err(Error::eval("Cannot use `<` with non-integer types"));
                }
            },
            T![>] => match (lval, rval) {
                (Value::Integer(l), Value::Integer(r)) => Value::Boolean(l > r),
                _ => {
                    return Err(Error::eval("Cannot use `>` with non-integer types"));
                }
            },
            T![==] => Value::Boolean(lval == rval),
            T![!=] => Value::Boolean(lval != rval),
            _ => {
                return Err(Error::eval(format!("Unexpected infix operator: {:?}", op)));
            }
        };

        Ok(v)
    }

    fn eval_id(&self, name: &String, env: &mut Env) -> Result<Value> {
        env.get(name)
            .or_else(|| Builtins::lookup(name).map(|b| Value::Builtin(b)))
            .ok_or(Error::eval(format!("Undefined variable {}", name)))
    }

    fn eval_call(
        &self,
        func: &Rc<Expression>,
        args: &Vec<Rc<Expression>>,
        env: &mut Env,
    ) -> Result<Value> {
        let func_val = self.eval_expr(func, env)?;
        match func_val {
            Value::Function {
                params,
                body,
                closure,
            } => {
                let mut fn_env = Env::new_enclosed(closure);
                if params.len() != args.len() {
                    return Err(Error::eval(format!(
                        "Function expected {} args, but got {}",
                        params.len(),
                        args.len()
                    )));
                }
                for (pname, arg) in params.iter().zip(args.iter()) {
                    let arg_val = self.eval_expr(arg, env)?;
                    fn_env.set(&pname.name, arg_val.unreturn());
                }
                self.eval_stmt(&body, &mut fn_env).map(|v| v.unreturn())
            }
            Value::Builtin(name) => {
                let arg_vals: Result<Vec<_>> = args
                    .iter()
                    .map(|a| self.eval_expr(a, env).map(|v| v.unreturn()))
                    .collect();

                Ok(match self.builtins.call(name, arg_vals?.as_slice()) {
                    Ok(val) => val,
                    Err(msg) => {
                        return Err(Error::eval(msg));
                    }
                })
            }
            _ => Err(Error::eval(format!(
                "{} is not a function, it cannot be called like one.",
                func_val
            ))),
        }
    }

    fn eval_if(
        &self,
        cond: &Rc<Expression>,
        consequence: &Rc<Statement>,
        alt: Option<&Rc<Statement>>,
        env: &mut Env,
    ) -> Result<Value> {
        if self.eval_expr(cond, env)?.is_truthy() {
            self.eval_stmt(consequence, env)
        } else {
            match alt {
                Some(a) => self.eval_stmt(a, env),
                None => Ok(Value::Null),
            }
        }
    }
}
