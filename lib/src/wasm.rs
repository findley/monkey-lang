use wasm_bindgen::prelude::*;
use wasm_bindgen::JsValue;

use super::Monkey;
use crate::eval::Value;

#[wasm_bindgen(js_name = Monkey)]
pub struct WasmMonkey {
    inner: Monkey,
}

#[wasm_bindgen(js_class = Monkey)]
impl WasmMonkey {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            inner: Monkey::new(),
        }
    }

    pub fn eval(&mut self, src: &str) -> std::result::Result<JsValue, JsError> {
        self.inner
            .eval(src)
            .map(|v| v.into())
            .map_err(|e| JsError::new(format!("{e}").as_str()))
    }

    pub fn reset(&mut self) {
        self.inner.reset();
    }

    pub fn register_print_handler(&mut self, printer: js_sys::Function) {
        self.inner.register_print_handler(Box::new(move |str| {
            let this = JsValue::null();
            let js_str = JsValue::from_str(str);
            printer
                .call1(&this, &js_str)
                .expect("Failed to call printer?");
        }));
    }
}

impl Into<JsValue> for Value {
    fn into(self) -> JsValue {
        match self {
            Value::Null => JsValue::NULL,
            Value::Boolean(v) => JsValue::from_bool(v),
            Value::String(v) => JsValue::from_str(&v),
            Value::Integer(v) => JsValue::from_f64(v as f64),
            Value::Return(v) => (*v).into(),
            Value::Function { .. } => JsValue::from_str("fn closure"),
            Value::Array(v) => {
                let js_array = v
                    .iter()
                    .cloned()
                    .map(|v| {
                        let jsv: JsValue = v.into();
                        jsv
                    })
                    .collect::<js_sys::Array>();
                JsValue::from(js_array)
            }
            Value::Builtin(v) => JsValue::from_str(format!("builtin:{v:?}").as_str()),
            Value::Hash(v) => {
                let js_obj = js_sys::Object::new();
                v.iter().for_each(|(k, v)| {
                    let js_key: JsValue = k.clone().into();
                    let js_val: JsValue = v.clone().into();
                    js_sys::Reflect::set(&js_obj, &js_key, &js_val).unwrap();
                });

                JsValue::from(js_obj)
            }
        }
    }
}
