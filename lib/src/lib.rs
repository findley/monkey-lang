mod error;
mod eval;
mod lexer;
mod parser;

#[cfg(target_arch = "wasm32")]
#[cfg(feature = "wasm")]
pub mod wasm;

pub use error::Error;
pub use eval::Env;
pub use eval::Value;

use eval::Evaluator;

pub type Result<T> = std::result::Result<T, Error>;

pub struct Monkey {
    eval: Evaluator,
    env: Env,
}

impl Monkey {
    pub fn new() -> Self {
        Self {
            eval: Evaluator::new(),
            env: Env::new(),
        }
    }

    pub fn eval(&mut self, src: &str) -> Result<Value> {
        let mut parser = parser::Parser::new(lexer::Lexer::from_source(&src));
        let program = parser.parse_program();
        if !parser.errors.is_empty() {
            return Err(Error::parser(parser.errors));
        }

        self.eval.eval(&parser::ast::Ast::Program(program), &mut self.env)
    }

    pub fn reset(&mut self) {
        self.env = Env::new();
    }

    pub fn register_print_handler(&mut self, printer: Box<dyn Fn(&str)>) {
        self.eval.set_printer(printer);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn print_handler() {
        let mut monkey = Monkey::new();
        let out = std::rc::Rc::new(std::cell::RefCell::new(String::new()));
        let out_inner = out.clone(); // Thanks rust =/
        monkey.register_print_handler(Box::new(move |s| {
            *out_inner.borrow_mut() = s.to_string();
        }));

        monkey.eval("puts(\"hello world\")").unwrap();

        assert_eq!("hello world", out.take())
    }
}
