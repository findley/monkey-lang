use std::fmt::Display;

#[derive(Debug)]
pub enum Error {
    ParseError(Vec<String>),
    EvalError(String),
}

impl Error {
    pub fn eval<S: AsRef<str>>(message: S) -> Self {
        Self::EvalError(message.as_ref().to_string())
    }

    pub fn parser(errors: Vec<crate::parser::Error>) -> Self {
        Self::ParseError(
            errors
                .into_iter()
                .map(|e| e.message)
                .collect::<Vec<String>>(),
        )
    }

    pub fn first_message(&self) -> String {
        match self {
            Self::ParseError(msgs) => msgs.first().expect("PaserError has 0 messages"),
            Self::EvalError(msg) => msg,
        }.to_string()
    }
}

impl std::error::Error for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ParseError(msgs) => {
                let error_messages = msgs
                    .iter()
                    .map(|msg| "    ".to_string() + msg)
                    .collect::<Vec<String>>()
                    .join("\n");
                write!(f, "Errors:\n{}", error_messages)
            }
            Self::EvalError(msg) => write!(f, "Error: {}", msg),
        }
    }
}
