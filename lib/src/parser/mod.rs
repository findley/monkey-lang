pub mod ast;

#[cfg(test)]
mod tests;

use std::rc::Rc;

use crate::T;

use super::lexer::{Lexer, Token, TokenKind};
use ast::*;

pub struct Parser<'source> {
    lexer: Lexer<'source>,
    curr_token: Token,
    peek_token: Token,
    pub errors: Vec<Error>,
}

#[derive(Debug)]
pub struct Error {
    pub message: String,
    pub start_pos: u32,
    pub end_pos: u32,
}

const BP_LOWEST: u8 = 1;
const BP_EQUALS: u8 = 2;
const BP_LESSGREATER: u8 = 3;
const BP_SUM: u8 = 4;
const BP_PRODUCT: u8 = 5;
const BP_PREFIX: u8 = 6;
const BP_CALL: u8 = 7;
const BP_INDEX: u8 = 8;

fn infix_binding_power(kind: TokenKind) -> u8 {
    match kind {
        T![==] => BP_EQUALS,
        T![!=] => BP_EQUALS,
        T![<] => BP_LESSGREATER,
        T![>] => BP_LESSGREATER,
        T![+] => BP_SUM,
        T![-] => BP_SUM,
        T![*] => BP_PRODUCT,
        T![/] => BP_PRODUCT,
        T![%] => BP_PRODUCT,
        T!['('] => BP_CALL,
        T!['['] => BP_INDEX,
        _ => BP_LOWEST,
    }
}

impl<'source> Parser<'source> {
    pub fn new(l: Lexer<'source>) -> Self {
        let mut p = Parser {
            lexer: l,
            curr_token: Token::default(),
            peek_token: Token::default(),
            errors: Vec::new(),
        };

        p.next_token();
        p.next_token();

        p
    }

    fn next_token(&mut self) -> Token {
        self.curr_token = self.peek_token;
        self.peek_token = self.lexer.next();

        self.curr_token
    }

    fn eat(&mut self, kind: TokenKind) -> bool {
        if self.curr_token.kind != kind {
            return false;
        }

        self.next_token();
        true
    }

    fn expect(&mut self, kind: TokenKind) -> Token {
        if self.curr_token.kind == kind {
            let t = self.curr_token;
            self.next_token();
            return t;
        }

        self.error(format!(
            "Expected {:?}, but encountered {:?}",
            kind, self.curr_token.kind
        ));
        return Token::new(
            TokenKind::Error,
            self.curr_token.start_pos,
            self.curr_token.end_pos,
        );
    }

    fn error(&mut self, message: String) {
        self.errors.push(Error {
            message,
            start_pos: self.curr_token.start_pos,
            end_pos: self.curr_token.end_pos,
        });
    }

    fn err_and_bump(&mut self, message: String) {
        self.error(message);
        self.next_token();
    }

    pub fn parse_program(&mut self) -> Program {
        let stmts = self.parse_statements();

        Program { statements: stmts }
    }

    fn parse_statements(&mut self) -> Vec<Rc<Statement>> {
        let mut stmts: Vec<Rc<Statement>> = Vec::new();

        while let Some(stmt) = self.parse_statement() {
            stmts.push(stmt);
        }

        stmts
    }

    fn parse_statement(&mut self) -> Option<Rc<Statement>> {
        match self.curr_token.kind {
            T![LET] => Some(self.parse_let_statement()),
            T![RETURN] => Some(self.parse_return_statement()),
            T![EOF] => None,
            T!['}'] => None,
            _ => Some(self.parse_expression_statement()),
        }
    }

    fn parse_let_statement(&mut self) -> Rc<Statement> {
        self.expect(T![LET]);
        let token = self.expect(T![ID]);
        let name = self.lexer.get_token_source(&token).to_string();
        self.expect(T![=]);
        let value = self.parse_expression(BP_LOWEST);
        self.eat(T![;]);

        Rc::new(Statement::LetStatement { token, name, value })
    }

    fn parse_return_statement(&mut self) -> Rc<Statement> {
        self.next_token(); // Eat the return keyword
        let value = self.parse_expression(BP_LOWEST);
        self.eat(T![;]);
        Rc::new(Statement::ReturnStatement(value))
    }

    fn parse_block_statement(&mut self) -> Rc<Statement> {
        self.expect(T!['{']);
        let statements = self.parse_statements();
        self.expect(T!['}']);

        Rc::new(Statement::BlockStatement { statements })
    }

    fn parse_expression_statement(&mut self) -> Rc<Statement> {
        let s = Rc::new(Statement::ExpressionStatement(
            self.parse_expression(BP_LOWEST),
        ));

        self.eat(T![;]);

        return s;
    }

    fn parse_expression(&mut self, bp: u8) -> Rc<Expression> {
        let mut left_expr = self.parse_prefix_expression();

        while bp < infix_binding_power(self.curr_token.kind) {
            let infix_expr = self.parse_infix(left_expr.clone());
            if let Some(expr) = infix_expr {
                left_expr = expr;
            } else {
                return left_expr;
            }
        }

        left_expr
    }

    fn parse_prefix_expression(&mut self) -> Rc<Expression> {
        match self.curr_token.kind {
            T![ID] => self.parse_identifier(),
            T![INT] => self.parse_int_expression(),
            T![STR] => self.parse_string_expression(),
            T!['('] => self.parse_grouped_expression(),
            T!['['] => self.parse_array_expression(),
            T!['{'] => self.parse_hash_expression(),
            T![IF] => self.parse_if_expression(),
            T![FN] => self.parse_function_expression(),
            T![TRUE] | T![FALSE] => self.parse_bool_expression(),
            T![!] | T![-] => {
                let op = self.curr_token;
                self.next_token();
                let right = self.parse_expression(BP_PREFIX);

                Rc::new(Expression::PrefixExpression { op, right })
            }
            _ => {
                self.err_and_bump(format!(
                    "Unexpected prefix token: {:?}",
                    self.curr_token.kind
                ));
                Rc::new(Expression::ErrorExpression)
            }
        }
    }

    fn parse_infix(&mut self, left: Rc<Expression>) -> Option<Rc<Expression>> {
        match self.curr_token.kind {
            T![+] | T![-] | T![/] | T![*] | T![==] | T![!=] | T![<] | T![>] | T![%] => {
                Some(Rc::new(self.parse_infix_operator(left)))
            }
            T!['('] => Some(self.parse_call_expression(left)),
            T!['['] => Some(self.parse_index_expression(left)),
            _ => None,
        }
    }

    fn parse_infix_operator(&mut self, left: Rc<Expression>) -> Expression {
        let op = self.curr_token;
        let bp = infix_binding_power(self.curr_token.kind);
        self.next_token();
        let right = self.parse_expression(bp);

        Expression::InfixExpression { op, left, right }
    }

    fn parse_grouped_expression(&mut self) -> Rc<Expression> {
        self.expect(T!['(']);
        let exp = self.parse_expression(BP_LOWEST);
        self.expect(T![')']);

        exp
    }

    fn parse_array_expression(&mut self) -> Rc<Expression> {
        self.expect(T!['[']);
        let elements = self.parse_expression_list();
        self.expect(T![']']);

        Rc::new(Expression::ArrayExpression { elements })
    }

    fn parse_hash_expression(&mut self) -> Rc<Expression> {
        let mut pairs: Vec<(Rc<Expression>, Rc<Expression>)> = Vec::new();

        self.expect(T!['{']);
        while self.curr_token.kind != T!['}'] {
            let key = self.parse_expression(BP_LOWEST);
            self.expect(T![:]);
            let value = self.parse_expression(BP_LOWEST);
            pairs.push((key, value));
            if !self.eat(T![,]) {
                break;
            }
        }
        self.expect(T!['}']);

        return Rc::new(Expression::HashExpression { pairs });
    }

    fn parse_if_expression(&mut self) -> Rc<Expression> {
        self.expect(T![IF]);
        self.expect(T!['(']);
        let condition = self.parse_expression(BP_LOWEST);
        self.expect(T![')']);
        let consequence = self.parse_block_statement();

        let mut alternative = None;
        if self.eat(T![ELSE]) {
            alternative = Some(self.parse_block_statement());
        }

        Rc::new(Expression::IfExpression {
            condition,
            consequence,
            alternative,
        })
    }

    fn parse_function_expression(&mut self) -> Rc<Expression> {
        self.expect(T![FN]);
        self.expect(T!['(']);
        let params = self.parse_parameter_list();
        self.expect(T![')']);
        let body = self.parse_block_statement();

        Rc::new(Expression::FunctionExpression { params, body })
    }

    fn parse_parameter_list(&mut self) -> Rc<Vec<Identifier>> {
        let mut ids = Vec::<Identifier>::new();

        if self.curr_token.kind == T![')'] {
            return Rc::new(ids);
        }

        let mut token = self.expect(T![ID]);
        ids.push(Identifier {
            token,
            name: self.lexer.get_token_source(&token).to_string(),
        });

        while self.eat(T![,]) {
            token = self.expect(T![ID]);
            ids.push(Identifier {
                token,
                name: self.lexer.get_token_source(&token).to_string(),
            });
        }

        Rc::new(ids)
    }

    fn parse_call_expression(&mut self, function: Rc<Expression>) -> Rc<Expression> {
        self.expect(T!['(']);
        let args = self.parse_expression_list();
        self.expect(T![')']);

        Rc::new(Expression::CallExpression { function, args })
    }

    fn parse_index_expression(&mut self, arr: Rc<Expression>) -> Rc<Expression> {
        self.expect(T!['[']);
        let index = self.parse_expression(BP_LOWEST);
        self.expect(T![']']);
        Rc::new(Expression::IndexExpression { left: arr, index })
    }

    fn parse_expression_list(&mut self) -> Vec<Rc<Expression>> {
        let mut args = Vec::<Rc<Expression>>::new();

        if self.curr_token.kind == T![')'] || self.curr_token.kind == T![']'] {
            return args;
        }

        args.push(self.parse_expression(BP_LOWEST));

        while self.eat(T![,]) {
            args.push(self.parse_expression(BP_LOWEST));
        }

        args
    }

    fn parse_identifier(&mut self) -> Rc<Expression> {
        let token = self.expect(T![ID]);
        let name = self.lexer.get_token_source(&token).to_string();

        Rc::new(Expression::IdentifierExpression(Identifier { token, name }))
    }

    fn parse_int_expression(&mut self) -> Rc<Expression> {
        let token = self.expect(T![INT]);
        let src = self.lexer.get_token_source(&token);
        let value: i64 = src.parse().unwrap();
        Rc::new(Expression::IntExpression { token, value })
    }

    fn parse_string_expression(&mut self) -> Rc<Expression> {
        let token = self.expect(T![STR]);

        let value = unescape_string(self.lexer.get_token_source(&token)).unwrap_or_else(|msg| {
            self.error(msg);
            "".to_string()
        });

        Rc::new(Expression::StringExpression { token, value })
    }

    fn parse_bool_expression(&mut self) -> Rc<Expression> {
        let token = self.curr_token;
        self.next_token();
        let value: bool = token.kind == T![TRUE];
        Rc::new(Expression::BooleanExpression { token, value })
    }
}

fn unescape_string(src: &str) -> Result<String, String> {
    let inner = &src[1..src.len() - 1]; // Remove enclosing quotes

    let mut val = String::with_capacity(src.len());
    let mut chars = inner.chars();
    while let Some(c) = chars.next() {
        let r = match c {
            '\\' => match chars.next() {
                Some('n') => '\n',
                Some('\\') => '\\',
                Some('"') => '"',
                Some(c) => {
                    return Err(format!("Unexpected escape sequence with \\{c}"));
                }
                None => {
                    return Err(format!("Unexpected EOF in escape sequence"));
                }
            },
            c => c,
        };
        val.push(r);
    }

    Ok(val)
}
