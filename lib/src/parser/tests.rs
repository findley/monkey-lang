use super::*;

use expect_test::{expect, Expect};

#[test]
fn return_statements() {
    let input = r#"
    return 5;
    return 10;
    return 993322;
    "#;
    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ReturnStatement(
                        IntExpression {
                            token: Token {
                                kind: Int,
                                start_pos: 12,
                                end_pos: 13,
                            },
                            value: 5,
                        },
                    ),
                    ReturnStatement(
                        IntExpression {
                            token: Token {
                                kind: Int,
                                start_pos: 26,
                                end_pos: 28,
                            },
                            value: 10,
                        },
                    ),
                    ReturnStatement(
                        IntExpression {
                            token: Token {
                                kind: Int,
                                start_pos: 41,
                                end_pos: 47,
                            },
                            value: 993322,
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn basic_let_statements() {
    let input = r#"
    let x = 5;
    let y = 10;
    let foobar = 838383;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    LetStatement {
                        token: Token {
                            kind: Ident,
                            start_pos: 9,
                            end_pos: 10,
                        },
                        name: "x",
                        value: IntExpression {
                            token: Token {
                                kind: Int,
                                start_pos: 13,
                                end_pos: 14,
                            },
                            value: 5,
                        },
                    },
                    LetStatement {
                        token: Token {
                            kind: Ident,
                            start_pos: 24,
                            end_pos: 25,
                        },
                        name: "y",
                        value: IntExpression {
                            token: Token {
                                kind: Int,
                                start_pos: 28,
                                end_pos: 30,
                            },
                            value: 10,
                        },
                    },
                    LetStatement {
                        token: Token {
                            kind: Ident,
                            start_pos: 40,
                            end_pos: 46,
                        },
                        name: "foobar",
                        value: IntExpression {
                            token: Token {
                                kind: Int,
                                start_pos: 49,
                                end_pos: 55,
                            },
                            value: 838383,
                        },
                    },
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn identifier_expression_statement() {
    let input = r#"
        foobar;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        IdentifierExpression(
                            Identifier {
                                token: Token {
                                    kind: Ident,
                                    start_pos: 9,
                                    end_pos: 15,
                                },
                                name: "foobar",
                            },
                        ),
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn int_expression_statement() {
    let input = r#"
        5;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        IntExpression {
                            token: Token {
                                kind: Int,
                                start_pos: 9,
                                end_pos: 10,
                            },
                            value: 5,
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn string_expression_statement() {
    let input = r#"
        "hello world!";
        "\\";
        "\\\"";
        "this is a \"test\"";
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        StringExpression {
                            token: Token {
                                kind: String,
                                start_pos: 9,
                                end_pos: 23,
                            },
                            value: "hello world!",
                        },
                    ),
                    ExpressionStatement(
                        StringExpression {
                            token: Token {
                                kind: String,
                                start_pos: 33,
                                end_pos: 37,
                            },
                            value: "\\",
                        },
                    ),
                    ExpressionStatement(
                        StringExpression {
                            token: Token {
                                kind: String,
                                start_pos: 47,
                                end_pos: 53,
                            },
                            value: "\\\"",
                        },
                    ),
                    ExpressionStatement(
                        StringExpression {
                            token: Token {
                                kind: String,
                                start_pos: 63,
                                end_pos: 83,
                            },
                            value: "this is a \"test\"",
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn string_expression_bad_escape_seq() {
    let input = r#""\q""#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        StringExpression {
                            token: Token {
                                kind: String,
                                start_pos: 0,
                                end_pos: 4,
                            },
                            value: "",
                        },
                    ),
                ],
            }
            Errors: [Error { message: "Unexpected escape sequence with \\q", start_pos: 4, end_pos: 4 }]"#]],
    );
}

#[test]
fn prefix_expressions() {
    let input = r#"
        !5;
        -15;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        PrefixExpression {
                            op: Token {
                                kind: Bang,
                                start_pos: 9,
                                end_pos: 10,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 10,
                                    end_pos: 11,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        PrefixExpression {
                            op: Token {
                                kind: Minus,
                                start_pos: 21,
                                end_pos: 22,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 22,
                                    end_pos: 24,
                                },
                                value: 15,
                            },
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn infix_expressions() {
    let input = r#"
        5 + 5;
        5 - 5;
        5 * 5;
        5 / 5;
        5 > 5;
        5 < 5;
        5 == 5;
        5 != 5;
        5 % 5;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Plus,
                                start_pos: 11,
                                end_pos: 12,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 9,
                                    end_pos: 10,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 13,
                                    end_pos: 14,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Minus,
                                start_pos: 26,
                                end_pos: 27,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 24,
                                    end_pos: 25,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 28,
                                    end_pos: 29,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Asterisk,
                                start_pos: 41,
                                end_pos: 42,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 39,
                                    end_pos: 40,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 43,
                                    end_pos: 44,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Slash,
                                start_pos: 56,
                                end_pos: 57,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 54,
                                    end_pos: 55,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 58,
                                    end_pos: 59,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Gt,
                                start_pos: 71,
                                end_pos: 72,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 69,
                                    end_pos: 70,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 73,
                                    end_pos: 74,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Lt,
                                start_pos: 86,
                                end_pos: 87,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 84,
                                    end_pos: 85,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 88,
                                    end_pos: 89,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Eq,
                                start_pos: 101,
                                end_pos: 103,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 99,
                                    end_pos: 100,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 104,
                                    end_pos: 105,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: NotEq,
                                start_pos: 117,
                                end_pos: 119,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 115,
                                    end_pos: 116,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 120,
                                    end_pos: 121,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Mod,
                                start_pos: 133,
                                end_pos: 134,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 131,
                                    end_pos: 132,
                                },
                                value: 5,
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 135,
                                    end_pos: 136,
                                },
                                value: 5,
                            },
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn nested_expressions() {
    let input = r#"
        -5 + 5;
        5 > 4 == 3 < 4;
        1 + 2 + 3;
        1 + 2 * 3;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Plus,
                                start_pos: 12,
                                end_pos: 13,
                            },
                            left: PrefixExpression {
                                op: Token {
                                    kind: Minus,
                                    start_pos: 9,
                                    end_pos: 10,
                                },
                                right: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 10,
                                        end_pos: 11,
                                    },
                                    value: 5,
                                },
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 14,
                                    end_pos: 15,
                                },
                                value: 5,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Eq,
                                start_pos: 31,
                                end_pos: 33,
                            },
                            left: InfixExpression {
                                op: Token {
                                    kind: Gt,
                                    start_pos: 27,
                                    end_pos: 28,
                                },
                                left: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 25,
                                        end_pos: 26,
                                    },
                                    value: 5,
                                },
                                right: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 29,
                                        end_pos: 30,
                                    },
                                    value: 4,
                                },
                            },
                            right: InfixExpression {
                                op: Token {
                                    kind: Lt,
                                    start_pos: 36,
                                    end_pos: 37,
                                },
                                left: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 34,
                                        end_pos: 35,
                                    },
                                    value: 3,
                                },
                                right: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 38,
                                        end_pos: 39,
                                    },
                                    value: 4,
                                },
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Plus,
                                start_pos: 55,
                                end_pos: 56,
                            },
                            left: InfixExpression {
                                op: Token {
                                    kind: Plus,
                                    start_pos: 51,
                                    end_pos: 52,
                                },
                                left: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 49,
                                        end_pos: 50,
                                    },
                                    value: 1,
                                },
                                right: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 53,
                                        end_pos: 54,
                                    },
                                    value: 2,
                                },
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 57,
                                    end_pos: 58,
                                },
                                value: 3,
                            },
                        },
                    ),
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Plus,
                                start_pos: 70,
                                end_pos: 71,
                            },
                            left: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 68,
                                    end_pos: 69,
                                },
                                value: 1,
                            },
                            right: InfixExpression {
                                op: Token {
                                    kind: Asterisk,
                                    start_pos: 74,
                                    end_pos: 75,
                                },
                                left: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 72,
                                        end_pos: 73,
                                    },
                                    value: 2,
                                },
                                right: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 76,
                                        end_pos: 77,
                                    },
                                    value: 3,
                                },
                            },
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn booleans_expressions() {
    let input = r#"
        true;
        false;
        let foobar = true;
        let barfoo = false;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        BooleanExpression {
                            token: Token {
                                kind: True,
                                start_pos: 9,
                                end_pos: 13,
                            },
                            value: true,
                        },
                    ),
                    ExpressionStatement(
                        BooleanExpression {
                            token: Token {
                                kind: False,
                                start_pos: 23,
                                end_pos: 28,
                            },
                            value: false,
                        },
                    ),
                    LetStatement {
                        token: Token {
                            kind: Ident,
                            start_pos: 42,
                            end_pos: 48,
                        },
                        name: "foobar",
                        value: BooleanExpression {
                            token: Token {
                                kind: True,
                                start_pos: 51,
                                end_pos: 55,
                            },
                            value: true,
                        },
                    },
                    LetStatement {
                        token: Token {
                            kind: Ident,
                            start_pos: 69,
                            end_pos: 75,
                        },
                        name: "barfoo",
                        value: BooleanExpression {
                            token: Token {
                                kind: False,
                                start_pos: 78,
                                end_pos: 83,
                            },
                            value: false,
                        },
                    },
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn grouped_expressions() {
    let input = r#"
        (1 + 2) * 3;
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Asterisk,
                                start_pos: 17,
                                end_pos: 18,
                            },
                            left: InfixExpression {
                                op: Token {
                                    kind: Plus,
                                    start_pos: 12,
                                    end_pos: 13,
                                },
                                left: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 10,
                                        end_pos: 11,
                                    },
                                    value: 1,
                                },
                                right: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 14,
                                        end_pos: 15,
                                    },
                                    value: 2,
                                },
                            },
                            right: IntExpression {
                                token: Token {
                                    kind: Int,
                                    start_pos: 19,
                                    end_pos: 20,
                                },
                                value: 3,
                            },
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn if_expressions() {
    let input = r#"
        if (x < y) { x }
        if (x < y) { x } else { y }
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        IfExpression {
                            condition: InfixExpression {
                                op: Token {
                                    kind: Lt,
                                    start_pos: 15,
                                    end_pos: 16,
                                },
                                left: IdentifierExpression(
                                    Identifier {
                                        token: Token {
                                            kind: Ident,
                                            start_pos: 13,
                                            end_pos: 14,
                                        },
                                        name: "x",
                                    },
                                ),
                                right: IdentifierExpression(
                                    Identifier {
                                        token: Token {
                                            kind: Ident,
                                            start_pos: 17,
                                            end_pos: 18,
                                        },
                                        name: "y",
                                    },
                                ),
                            },
                            consequence: BlockStatement {
                                statements: [
                                    ExpressionStatement(
                                        IdentifierExpression(
                                            Identifier {
                                                token: Token {
                                                    kind: Ident,
                                                    start_pos: 22,
                                                    end_pos: 23,
                                                },
                                                name: "x",
                                            },
                                        ),
                                    ),
                                ],
                            },
                            alternative: None,
                        },
                    ),
                    ExpressionStatement(
                        IfExpression {
                            condition: InfixExpression {
                                op: Token {
                                    kind: Lt,
                                    start_pos: 40,
                                    end_pos: 41,
                                },
                                left: IdentifierExpression(
                                    Identifier {
                                        token: Token {
                                            kind: Ident,
                                            start_pos: 38,
                                            end_pos: 39,
                                        },
                                        name: "x",
                                    },
                                ),
                                right: IdentifierExpression(
                                    Identifier {
                                        token: Token {
                                            kind: Ident,
                                            start_pos: 42,
                                            end_pos: 43,
                                        },
                                        name: "y",
                                    },
                                ),
                            },
                            consequence: BlockStatement {
                                statements: [
                                    ExpressionStatement(
                                        IdentifierExpression(
                                            Identifier {
                                                token: Token {
                                                    kind: Ident,
                                                    start_pos: 47,
                                                    end_pos: 48,
                                                },
                                                name: "x",
                                            },
                                        ),
                                    ),
                                ],
                            },
                            alternative: Some(
                                BlockStatement {
                                    statements: [
                                        ExpressionStatement(
                                            IdentifierExpression(
                                                Identifier {
                                                    token: Token {
                                                        kind: Ident,
                                                        start_pos: 58,
                                                        end_pos: 59,
                                                    },
                                                    name: "y",
                                                },
                                            ),
                                        ),
                                    ],
                                },
                            ),
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn fn_expressions() {
    let input = r#"
        fn() {};
        fn(x, y) { x + y; };
        fn(x, y, z) {};
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        FunctionExpression {
                            params: [],
                            body: BlockStatement {
                                statements: [],
                            },
                        },
                    ),
                    ExpressionStatement(
                        FunctionExpression {
                            params: [
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 29,
                                        end_pos: 30,
                                    },
                                    name: "x",
                                },
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 32,
                                        end_pos: 33,
                                    },
                                    name: "y",
                                },
                            ],
                            body: BlockStatement {
                                statements: [
                                    ExpressionStatement(
                                        InfixExpression {
                                            op: Token {
                                                kind: Plus,
                                                start_pos: 39,
                                                end_pos: 40,
                                            },
                                            left: IdentifierExpression(
                                                Identifier {
                                                    token: Token {
                                                        kind: Ident,
                                                        start_pos: 37,
                                                        end_pos: 38,
                                                    },
                                                    name: "x",
                                                },
                                            ),
                                            right: IdentifierExpression(
                                                Identifier {
                                                    token: Token {
                                                        kind: Ident,
                                                        start_pos: 41,
                                                        end_pos: 42,
                                                    },
                                                    name: "y",
                                                },
                                            ),
                                        },
                                    ),
                                ],
                            },
                        },
                    ),
                    ExpressionStatement(
                        FunctionExpression {
                            params: [
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 58,
                                        end_pos: 59,
                                    },
                                    name: "x",
                                },
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 61,
                                        end_pos: 62,
                                    },
                                    name: "y",
                                },
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 64,
                                        end_pos: 65,
                                    },
                                    name: "z",
                                },
                            ],
                            body: BlockStatement {
                                statements: [],
                            },
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn call_expressions() {
    let input = r#"
        add(1, 2 * 3, 4 + 5);
        factory()(1, 2 * 3, 4 + 5);
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        CallExpression {
                            function: IdentifierExpression(
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 9,
                                        end_pos: 12,
                                    },
                                    name: "add",
                                },
                            ),
                            args: [
                                IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 13,
                                        end_pos: 14,
                                    },
                                    value: 1,
                                },
                                InfixExpression {
                                    op: Token {
                                        kind: Asterisk,
                                        start_pos: 18,
                                        end_pos: 19,
                                    },
                                    left: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 16,
                                            end_pos: 17,
                                        },
                                        value: 2,
                                    },
                                    right: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 20,
                                            end_pos: 21,
                                        },
                                        value: 3,
                                    },
                                },
                                InfixExpression {
                                    op: Token {
                                        kind: Plus,
                                        start_pos: 25,
                                        end_pos: 26,
                                    },
                                    left: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 23,
                                            end_pos: 24,
                                        },
                                        value: 4,
                                    },
                                    right: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 27,
                                            end_pos: 28,
                                        },
                                        value: 5,
                                    },
                                },
                            ],
                        },
                    ),
                    ExpressionStatement(
                        CallExpression {
                            function: CallExpression {
                                function: IdentifierExpression(
                                    Identifier {
                                        token: Token {
                                            kind: Ident,
                                            start_pos: 39,
                                            end_pos: 46,
                                        },
                                        name: "factory",
                                    },
                                ),
                                args: [],
                            },
                            args: [
                                IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 49,
                                        end_pos: 50,
                                    },
                                    value: 1,
                                },
                                InfixExpression {
                                    op: Token {
                                        kind: Asterisk,
                                        start_pos: 54,
                                        end_pos: 55,
                                    },
                                    left: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 52,
                                            end_pos: 53,
                                        },
                                        value: 2,
                                    },
                                    right: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 56,
                                            end_pos: 57,
                                        },
                                        value: 3,
                                    },
                                },
                                InfixExpression {
                                    op: Token {
                                        kind: Plus,
                                        start_pos: 61,
                                        end_pos: 62,
                                    },
                                    left: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 59,
                                            end_pos: 60,
                                        },
                                        value: 4,
                                    },
                                    right: IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 63,
                                            end_pos: 64,
                                        },
                                        value: 5,
                                    },
                                },
                            ],
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn array_expressions() {
    let input = r#"
    [];
    [1, true, "three"];
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        ArrayExpression {
                            elements: [],
                        },
                    ),
                    ExpressionStatement(
                        ArrayExpression {
                            elements: [
                                IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 14,
                                        end_pos: 15,
                                    },
                                    value: 1,
                                },
                                BooleanExpression {
                                    token: Token {
                                        kind: True,
                                        start_pos: 17,
                                        end_pos: 21,
                                    },
                                    value: true,
                                },
                                StringExpression {
                                    token: Token {
                                        kind: String,
                                        start_pos: 23,
                                        end_pos: 30,
                                    },
                                    value: "three",
                                },
                            ],
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn array_index_expressions() {
    let input = r#"
    myArray[1+1];
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        IndexExpression {
                            left: IdentifierExpression(
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 5,
                                        end_pos: 12,
                                    },
                                    name: "myArray",
                                },
                            ),
                            index: InfixExpression {
                                op: Token {
                                    kind: Plus,
                                    start_pos: 14,
                                    end_pos: 15,
                                },
                                left: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 13,
                                        end_pos: 14,
                                    },
                                    value: 1,
                                },
                                right: IntExpression {
                                    token: Token {
                                        kind: Int,
                                        start_pos: 15,
                                        end_pos: 16,
                                    },
                                    value: 1,
                                },
                            },
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn array_index_precedence() {
    let input = r#"
    a * [1, 2, 3, 4][b * c] * d
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        InfixExpression {
                            op: Token {
                                kind: Asterisk,
                                start_pos: 29,
                                end_pos: 30,
                            },
                            left: InfixExpression {
                                op: Token {
                                    kind: Asterisk,
                                    start_pos: 7,
                                    end_pos: 8,
                                },
                                left: IdentifierExpression(
                                    Identifier {
                                        token: Token {
                                            kind: Ident,
                                            start_pos: 5,
                                            end_pos: 6,
                                        },
                                        name: "a",
                                    },
                                ),
                                right: IndexExpression {
                                    left: ArrayExpression {
                                        elements: [
                                            IntExpression {
                                                token: Token {
                                                    kind: Int,
                                                    start_pos: 10,
                                                    end_pos: 11,
                                                },
                                                value: 1,
                                            },
                                            IntExpression {
                                                token: Token {
                                                    kind: Int,
                                                    start_pos: 13,
                                                    end_pos: 14,
                                                },
                                                value: 2,
                                            },
                                            IntExpression {
                                                token: Token {
                                                    kind: Int,
                                                    start_pos: 16,
                                                    end_pos: 17,
                                                },
                                                value: 3,
                                            },
                                            IntExpression {
                                                token: Token {
                                                    kind: Int,
                                                    start_pos: 19,
                                                    end_pos: 20,
                                                },
                                                value: 4,
                                            },
                                        ],
                                    },
                                    index: InfixExpression {
                                        op: Token {
                                            kind: Asterisk,
                                            start_pos: 24,
                                            end_pos: 25,
                                        },
                                        left: IdentifierExpression(
                                            Identifier {
                                                token: Token {
                                                    kind: Ident,
                                                    start_pos: 22,
                                                    end_pos: 23,
                                                },
                                                name: "b",
                                            },
                                        ),
                                        right: IdentifierExpression(
                                            Identifier {
                                                token: Token {
                                                    kind: Ident,
                                                    start_pos: 26,
                                                    end_pos: 27,
                                                },
                                                name: "c",
                                            },
                                        ),
                                    },
                                },
                            },
                            right: IdentifierExpression(
                                Identifier {
                                    token: Token {
                                        kind: Ident,
                                        start_pos: 31,
                                        end_pos: 32,
                                    },
                                    name: "d",
                                },
                            ),
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn hash_literal() {
    let input = r#"
    {"one": 1, "two": 2, "three": 3}
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        HashExpression {
                            pairs: [
                                (
                                    StringExpression {
                                        token: Token {
                                            kind: String,
                                            start_pos: 6,
                                            end_pos: 11,
                                        },
                                        value: "one",
                                    },
                                    IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 13,
                                            end_pos: 14,
                                        },
                                        value: 1,
                                    },
                                ),
                                (
                                    StringExpression {
                                        token: Token {
                                            kind: String,
                                            start_pos: 16,
                                            end_pos: 21,
                                        },
                                        value: "two",
                                    },
                                    IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 23,
                                            end_pos: 24,
                                        },
                                        value: 2,
                                    },
                                ),
                                (
                                    StringExpression {
                                        token: Token {
                                            kind: String,
                                            start_pos: 26,
                                            end_pos: 33,
                                        },
                                        value: "three",
                                    },
                                    IntExpression {
                                        token: Token {
                                            kind: Int,
                                            start_pos: 35,
                                            end_pos: 36,
                                        },
                                        value: 3,
                                    },
                                ),
                            ],
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

#[test]
fn hash_literal_with_expressions() {
    let input = r#"
    {"one": 0 + 1, "two": 10 - 8, "three": 15 / 5}
    "#;

    check_parse(
        input,
        expect![[r#"
            Program {
                statements: [
                    ExpressionStatement(
                        HashExpression {
                            pairs: [
                                (
                                    StringExpression {
                                        token: Token {
                                            kind: String,
                                            start_pos: 6,
                                            end_pos: 11,
                                        },
                                        value: "one",
                                    },
                                    InfixExpression {
                                        op: Token {
                                            kind: Plus,
                                            start_pos: 15,
                                            end_pos: 16,
                                        },
                                        left: IntExpression {
                                            token: Token {
                                                kind: Int,
                                                start_pos: 13,
                                                end_pos: 14,
                                            },
                                            value: 0,
                                        },
                                        right: IntExpression {
                                            token: Token {
                                                kind: Int,
                                                start_pos: 17,
                                                end_pos: 18,
                                            },
                                            value: 1,
                                        },
                                    },
                                ),
                                (
                                    StringExpression {
                                        token: Token {
                                            kind: String,
                                            start_pos: 20,
                                            end_pos: 25,
                                        },
                                        value: "two",
                                    },
                                    InfixExpression {
                                        op: Token {
                                            kind: Minus,
                                            start_pos: 30,
                                            end_pos: 31,
                                        },
                                        left: IntExpression {
                                            token: Token {
                                                kind: Int,
                                                start_pos: 27,
                                                end_pos: 29,
                                            },
                                            value: 10,
                                        },
                                        right: IntExpression {
                                            token: Token {
                                                kind: Int,
                                                start_pos: 32,
                                                end_pos: 33,
                                            },
                                            value: 8,
                                        },
                                    },
                                ),
                                (
                                    StringExpression {
                                        token: Token {
                                            kind: String,
                                            start_pos: 35,
                                            end_pos: 42,
                                        },
                                        value: "three",
                                    },
                                    InfixExpression {
                                        op: Token {
                                            kind: Slash,
                                            start_pos: 47,
                                            end_pos: 48,
                                        },
                                        left: IntExpression {
                                            token: Token {
                                                kind: Int,
                                                start_pos: 44,
                                                end_pos: 46,
                                            },
                                            value: 15,
                                        },
                                        right: IntExpression {
                                            token: Token {
                                                kind: Int,
                                                start_pos: 49,
                                                end_pos: 50,
                                            },
                                            value: 5,
                                        },
                                    },
                                ),
                            ],
                        },
                    ),
                ],
            }
            Errors: []"#]],
    );
}

fn check_parse(input: &str, expect: Expect) {
    let mut parser = Parser::new(Lexer::from_source(input));
    let program = parser.parse_program();
    let actual = format!("{:#?}\nErrors: {:?}", program, parser.errors);
    expect.assert_eq(&actual)
}
