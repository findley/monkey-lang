use std::rc::Rc;

use super::Token;

#[allow(dead_code)]
pub enum Ast {
    Program(Program),
    Statement(Statement),
    Expression(Rc<Expression>),
    Identifier(Identifier),
}

#[derive(Debug)]
pub struct Program {
    pub statements: Vec<Rc<Statement>>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Statement {
    LetStatement { token: Token, name: String, value: Rc<Expression> },
    ReturnStatement(Rc<Expression>),
    ExpressionStatement(Rc<Expression>),
    BlockStatement { statements: Vec<Rc<Statement>> },
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expression {
    ErrorExpression,
    IntExpression {
        token: Token,
        value: i64,
    },
    StringExpression {
        token: Token,
        value: String,
    },
    IdentifierExpression(Identifier),
    PrefixExpression {
        op: Token,
        right: Rc<Expression>,
    },
    InfixExpression {
        op: Token,
        left: Rc<Expression>,
        right: Rc<Expression>,
    },
    BooleanExpression {
        token: Token,
        value: bool,
    },
    IfExpression {
        condition: Rc<Expression>,
        consequence: Rc<Statement>,
        alternative: Option<Rc<Statement>>,
    },
    FunctionExpression {
        params: Rc<Vec<Identifier>>,
        body: Rc<Statement>,
    },
    CallExpression {
        function: Rc<Expression>,
        args: Vec<Rc<Expression>>,
    },
    ArrayExpression {
        elements: Vec<Rc<Expression>>,
    },
    HashExpression {
        pairs: Vec<(Rc<Expression>, Rc<Expression>)>,
    },
    IndexExpression {
        left: Rc<Expression>,
        index: Rc<Expression>,
    },
}

#[derive(Debug, Clone, PartialEq)]
pub struct Identifier {
    pub token: Token,
    pub name: String,
}
