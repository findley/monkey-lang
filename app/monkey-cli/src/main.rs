use getopts::Options;

use monkey::Monkey;

mod repl;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let args: Vec<String> = std::env::args().collect();
    let program = args[0].clone();
    let opts = make_opts();
    let matches = opts.parse(&args[1..])?;

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return Ok(());
    }

    if matches.opt_present("r") {
        return start_repl();
    }

    if matches.free.len() != 1 {
        print_usage(&program, opts);
        std::process::exit(1);
    }

    eval_file(&matches.free[0])
}

fn eval_file(file_name: &str) -> Result<()> {
    let src = std::fs::read_to_string(file_name)?;
    let mut monkey = Monkey::new();
    match monkey.eval(&src) {
        Ok(_) => Ok(()),
        Err(e) => {
            eprintln!("{e}");
            std::process::exit(2);
        }
    }
}

fn start_repl() -> Result<()> {
    println!(
        "Hello Nugget! This is the Monkey programming language!\nFeel free to type in commands"
    );
    repl::start(std::io::stdin().lines(), std::io::stdout())
}

fn make_opts() -> Options {
    let mut opts = Options::new();
    opts.optflag("r", "repl", "start an interactive REPL session");
    opts.optflag("h", "help", "print this help menu");

    opts
}

fn print_usage(program: &str, opts: Options) {
    let summary = format!("Usage: {program} [options] <FILE>");
    print!("{}", opts.usage(&summary));
}
