use std::error::Error;

use monkey::Monkey;

const PROMPT: &str = ">> ";

pub const MONKEY_FACE: &str = r#"            __,__
   .--.  .-"     "-.  .--.
  / .. \/  .-. .-.  \/ .. \
 | |  '|  /   Y   \  |'  | |
 | \   \  \ 0 | 0 /  /   / |
  \ '- ,\.-"""""""-./, -' /
   ''-' /_   ^ ^   _\ '-''
       |  \._   _./  |
       \   \ '~' /   /
        '._ '-=-' _.'
           '-----'
"#;

pub const MONKEY_BUSINESS: &str = "Woops! We ran into some monkey business here!";

pub fn start<I, O>(input: I, mut output: O) -> Result<(), Box<dyn Error>>
where
    I: IntoIterator<Item = std::io::Result<String>>,
    O: std::io::Write,
{
    write!(output, "{}", PROMPT)?;
    output.flush()?;

    let mut repl = Monkey::new();

    for line in input {
        let src_line = line?;
        let src = src_line.as_str();
        match repl.eval(src) {
            Ok(v) => writeln!(output, "{v}")?,
            Err(e) => writeln!(output, "{}\n{}\n{e}", MONKEY_FACE, MONKEY_BUSINESS)?,
        };

        write!(output, "{}", PROMPT)?;
        output.flush()?;
    }

    Ok(())
}
