# Monkey Lang Playground

This is a web based playground for monkey lang. The UI is built with solidjs and
tailwindcss and bundled with vite. The monkey interpreter is compiled to wasm
and used to evaluate the code.

<https://monkey.findley.dev>

## Development

Install npm dependencies
```bash
$ npm config set @findley:registry=https://gitlab.com/api/v4/packages/npm/
$ npm ci
```

Start dev mode
```bash
$ npm run dev
```

## Deploying

Every commit on the main branch will trigger a website build pipeline that will
publish a new docker image. There's also a manual job in the pipeline that can
be trigger to deploy the new image to <https://monkey.findley.dev>.
