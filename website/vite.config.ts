import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';
import wasmPackPlugin from './scripts/vite-plugin-wasm-pack'

export default defineConfig({
    plugins: [solidPlugin(), wasmPackPlugin(['monkey'])],
    server: {
        port: 3000,
    },
    build: {
        target: 'esnext',
    },
    optimizeDeps: {
        include: ['@codemirror/state', '@codemirror/view'],
    },
});
