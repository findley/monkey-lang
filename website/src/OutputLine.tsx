import { Component } from 'solid-js';

type MsgType = 'print' | 'error' | 'sys';

export type MonkeyMsg = {
    type: MsgType
    msg: string
};

type Props = {
    msg: MonkeyMsg,
}

const OutputLine: Component<Props> = (props) => {
    return (
        <div class={typeColor(props.msg.type)}>{props.msg.msg}</div>
    );
}

function typeColor(t: MsgType): string {
    switch (t) {
        case 'print':
            return 'text-gray-200';
        case 'sys':
            return 'text-gray-500';
        case 'error':
            return 'text-red-400';
    }
}

export default OutputLine;
