const examples = [
    {
        name: 'Hello World', src: 'puts("Hello world!");'
    },
    {
        name: 'Fizz Buzz',
        src: `let fizzbuzz = fn(n) {
    if (n == 1) {
        puts(1);
        return 0;
    }

    if (n % 15 == 0) {
        puts("fizzbuzz");
        return fizzbuzz(n-1);
    }
    if (n % 5 == 0) {
        puts("buzz");
        return fizzbuzz(n-1);
    }
    if (n % 3 == 0) {
        puts("fizz");
        return fizzbuzz(n-1);
    }

    puts(n);
    fizzbuzz(n-1);
};

fizzbuzz(100);`,
    },
    {
        name: 'Double with Map',
        src: `let map = fn(arr, f) {
    let iter = fn (arr, accumulated) {
        if (len(arr) == 0) {
            accumulated
        } else {
            iter(rest(arr), push(accumulated, f(first(arr))));
        }
    };
    iter(arr, []);
};

let a = [1, 2, 3, 4];
let double = fn(x) { x * 2 };

puts("Before double: ", a);
puts("After double: ", map(a, double));`
    }
];
export default examples;
