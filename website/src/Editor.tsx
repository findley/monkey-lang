import { createCodeMirror } from 'solid-codemirror';
import { Component, createEffect } from 'solid-js';
import { lineNumbers, keymap } from '@codemirror/view';
import { indentWithTab } from '@codemirror/commands';

type Props = {
    initialSrc: string,
    onChange: (v: string) => void,
    onRun: () => void,
}

const Editor: Component<Props> = (props) => {
    const { ref: editorRef, createExtension, editorView } = createCodeMirror({
        value: props.initialSrc,
        onValueChange: props.onChange,
    });

    createExtension(lineNumbers());
    createExtension(keymap.of([
        indentWithTab,
        {
            key: "Ctrl-Enter",
            run: () => {
                props.onRun();
                return true;
            },
            preventDefault: true,
        }
    ]));

    createEffect(() => {
        let view = editorView();
        if (view) {
            view.dispatch({
                changes: {
                    from: 0,
                    to: view.state.doc.length,
                    insert: props.initialSrc,
                },
            });
        }
    });

    return <div ref={editorRef} class="relative max-h-[65vh] mb-3 p-2 block whitespace-nowrap flex-grow outline-none bg-slate-700 text-gray-100 rounded-md text-sm">
        <div class="absolute bottom-2 right-2 z-10">
            <button onClick={props.onRun} title="ctrl+enter" class="px-4 py-1 rounded-md bg-blue-600 hover:bg-blue-500 text-gray-100">Run</button>
        </div>
    </div>;
};

export default Editor;
