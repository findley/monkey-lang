import { Component } from 'solid-js';

const Docs: Component = () => {
    return (
        <div>
            <h2 class="text-xl font-bold text-center">Docs</h2>
            <section>
                <h3>About</h3>
                <p><a href="https://monkeylang.org/">Monkey</a> is an educational programming language from the book <a href="https://interpreterbook.com/">Writing an Interpreter in Go</a> by <a href="https://thorstenball.com/">Thorston Ball</a></p>
                <p>This implementation of Monkey is written in Rust instead of Go, and it also has some extra features beyond what's covered in the book. It has been compiled to WASM so that you can try it out here in the browser.</p>
                <br />
                <p>Source code: <a href="https://gitlab.com/findley/monkey-lang">https://gitlab.com/findley/monkey-lang</a></p>
            </section>
            <section>
                <h3>Primitive Types</h3>
                <dl>
                    <dt>Integers</dt>
                    <dd>You can do all of the expected arithematic with integer numbers: <code>(1 + 2) * 3 / 4</code></dd>

                    <dt>Strings</dt>
                    <dd>You can concat them with the + operator:<br /><code>"Hello" + " " + "World!"</code></dd>

                    <dt>Booleans</dt>
                    <dd>Just <code>true</code> or <code>false</code></dd>

                    <dt>Arrays</dt>
                    <dd>
                        <p>
                            Lists of values<br />
                            <code>[1, 2, 3]</code><br />
                        </p>
                        <p>
                            They can hold mixed types<br />
                            <code>[true, "false", []]</code><br />
                        </p>
                        <p>
                            Index into them with the [] operator, they're 0 based<br />
                            <code>[1, 2, 3][0]</code><br />
                        </p>
                        <p>
                            Use the builtin push to add elements to an array<br />
                            <code>push([1, 2], 3)</code>
                        </p>
                    </dd>

                    <dt>Hashes</dt>
                    <dd>
                        These are just objects (or dictionaries). You can use integers, strings, and booleans as keys. Use the [] to get the value at a key.<br />
                        <code>{`{"name": "Marco", 1: true}["name"]`}</code>
                    </dd>
                </dl>
            </section>
            <section>
                <h3>Declaring Variables</h3>
                <p>Use the let keyword to bind new variables</p>
                <p><code>let x = 5;</code></p>
            </section>
            <section>
                <h3>Conditionals</h3>
                <p>If statements are expressions in Monkey, so you can assign them to variables.</p>
                <pre>
                    <code class="block">
{`let result = if (true) {
    10
} else {
    0
}`}
                    </code>
                </pre>
            </section>
            <section>
                <h3>Functions</h3>
                <p>In Monkey, functions are first-class citizens. That is, they are just expressions which can be bound to variables like any other type.</p>
                <pre>
                    <code class="block">{`let double = fn(x) { x * 2 };
double(2);`}
                    </code>
                </pre>
                <p>Functions capture the scope where they're defined as a closure.</p>
                <pre>
                    <code class="block">{`let x = 5;
let foo = fn() {
    puts(x);
};
foo();`}
                    </code>
                </pre>
            </section>
            <section>
                <h3>Builtins</h3>
                <dl>
                    <dt>puts</dt>
                    <dd>Print out the given values: <code>puts("Hello World!")</code></dd>

                    <dt>push</dt>
                    <dd>Append an element to the end of an array: <code>append(myArray, 5)</code><br />Returns a new array without modifying the original.</dd>

                    <dt>len</dt>
                    <dd>Gives the length of an array or a string: <code>len("hello"); len([1,2,3])</code></dd>

                    <dt>first</dt>
                    <dd>Gives the first element of an array, or null if the array is empty.</dd>

                    <dt>last</dt>
                    <dd>Gives the last element of an array, or null if the array is empty.</dd>

                    <dt>rest</dt>
                    <dd>Takes an array, and returns a new array with all of the elements except the first one. This can be useful for recursive functions.</dd>
                </dl>
            </section>
        </div>
    );
};

export default Docs;
