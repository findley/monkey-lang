import { Monkey } from '@findley/monkey';
import { Component, For, createSignal } from 'solid-js';

type Props = {
    monkey: Monkey,
}

const Repl: Component<Props> = (props) => {
    let ref: HTMLInputElement;
    const [output, setOutput] = createSignal<string[]>([]);

    function handleSubmit(event: Event) {
        event.preventDefault();
        if (ref.value.trim() === "") {
            return;
        }
        try {
            const result = props?.monkey?.eval(ref.value);
            setOutput([...output(), '› ' + ref.value, JSON.stringify(result)]);
        } catch(e) {
            console.log("error:",e);
            setOutput([...output(), '› ' + ref.value, e.toString()]);
        }
        ref.value = '';
    }

    function clear(e: Event) {
        e.preventDefault();
        setOutput([]);
    }

    return (
        <div>
            <div>
                <h2 class="text-xl font-bold text-center">REPL</h2>
            </div>
            <div>
                <For each={output()}>
                {(line: string) => (
                    <div class="font-mono">{line}</div>
                )}
                </For>
                <form onSubmit={handleSubmit} class="flex">
                    <span class="text-xl">&rsaquo; </span>
                    <input ref={ref} type="text" placeholder="Write an expression" class="mx-2 font-mono border-b-[1px] border-slate-700 flex-grow bg-slate-800 text-gray-100 outline-none" />
                    <button type="button" onClick={clear} class="px-3 py-1 rounded-md bg-blue-600 hover:bg-blue-500 text-gray-100">clear</button>
                </form>
            </div>
        </div>
    );
};

export default Repl;
