import type { Component } from 'solid-js';
import { createSignal, onMount, For } from 'solid-js';
import init, { Monkey } from '@findley/monkey';

import examples from './example_programs';
import Editor from './Editor';
import Docs from './Docs';
import Repl from './Repl';
import OutputLine, { MonkeyMsg } from './OutputLine';

const App: Component = () => {
    const [output, setOutput] = createSignal<MonkeyMsg[]>([]);
    const [src, setSrc] = createSignal<string>("");
    const [initialSrc, setInitialSrc] = createSignal<string>(examples[0].src);
    const [monkey, setMonkey] = createSignal<Monkey|null>(null);

    onMount(async () => {
        await init();
        setMonkey(new Monkey());
    });

    function handleRun() {
        let monkey = new Monkey();
        setOutput([]);
        monkey.register_print_handler((s: string) => setOutput([...output(), {type: 'print', msg: s}]));
        let t0 = performance.now();
        try {
            monkey.eval(src());
        } catch (e) {
            setOutput([...output(), {type: 'error', msg: e.toString()}]);
            return;
        }
        let t1 = performance.now();
        setOutput([...output(), {type: 'sys', msg: `Program completed in ${t1 - t0} ms`}]);
        setMonkey(monkey);
    }

    function loadExample(src: string) {
        setInitialSrc(""); // To ensure that the prop changes
        setInitialSrc(src);
    }

    return (
        <div class="h-screen flex flex-col">
            <h1 class="text-3xl text-center mb-3 text-indigo-200">Monkey Lang Playground</h1>
            <main class="flex flex-row flex-grow min-h-0">
                <div class="p-3 w-1/4 min-h-0 overflow-y-auto">
                    <Docs />
                </div>
                <div class="p-3 flex-grow flex flex-col">
                    <div class="flex">
                        <span class="mr-3 font-bold text-gray-300">Examples</span>
                        <For each={examples}>{ex => (
                            <button class="mr-6 text-blue-400 hover:text-blue-200" onClick={() => loadExample(ex.src)}>{ex.name}</button>
                        )}</For>
                    </div>
                    <Editor onChange={setSrc} initialSrc={initialSrc()} onRun={handleRun} />
                    <h2 class="mt-3 text-gray-100 text-lg font-bold">Output</h2>
                    <div class="relative p-2 mt-1 h-44 overflow-y-auto bg-slate-700 rounded-md font-mono">
                        <For each={output()}>
                        {(msg: MonkeyMsg) => (
                            <OutputLine msg={msg} />
                        )}
                        </For>
                        <button
                            class="absolute font-sans text-base bottom-2 right-2 z-10 px-4 py-1 rounded-md bg-blue-600 hover:bg-blue-500 text-gray-100"
                            onClick={() => setOutput([])}
                        >
                            clear
                        </button>
                    </div>
                </div>
                <div class="p-2 w-1/4">
                    <Repl monkey={monkey()} />
                </div>
            </main>
        </div>
    );
};

export default App;
